package Vector;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

public class VectorOperations<T> {
	
	Vector<T> list = new Vector<>();

	public void addItems(T item) {
		list.add(item);
	}

	public void addAtFirst(T num) {
		list.add(0, num);
		System.out.println("Number Added!");
	}

	public void addAtLast(T num) {
		list.add(list.size() - 1, num);
		System.out.println("Number Added at Last!");
	}

	public void addAtIndex(int n, T num) {
		if (n < list.size() && n >= 0)
			list.add(n, num);
		else
			System.out.println("Index Invalid!");
	}

	public void removeAtFirst() {
		if (list.isEmpty()) {
			System.out.println("Vector is Empty!");
			return;
		}
		
		System.out.println("Removed Number:" + list.remove(0));
	}

	public void removeAtLast() {
		if (list.isEmpty()) {
			System.out.println("Vector is Empty!");
			return;
		}
		System.out.println("Removed Number:" + 	list.remove(list.size() - 1));
	}

	public void removeAtIndex(int n) {
		if (list.isEmpty() || n < 0 || n > list.size()) {
			System.out.println("Invalid Index!");
			return;
		}
	
		System.out.println("Removed Number:" + list.remove(n));
	}

	public void searchNumber(T num) {
		List<Integer> indexes = new ArrayList<Integer>();
		Enumeration<T> en = list.elements();
		while(en.hasMoreElements())
			if (en.nextElement().equals(num)) {
				indexes.add(list.indexOf(en.nextElement()));
			}
		
		if (indexes.isEmpty())
			System.out.println("Number not found");

		else {
			for (Integer i : indexes)
				System.out.println(i.intValue());
		}

	}
	
	public void print(){
		System.out.println(list);
	}

}
