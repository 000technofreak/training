package Stack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class StackOperations<T> {
	
	Stack<T> list = new Stack<T>();

	public void addItems(T item) {
		list.push(item);
	}

	public void addAtFirst(T num) {
		list.push(num);
		System.out.println("Number Added!");
	}

	public void addAtLast(T num) {
		list.add(list.size() - 1, num);
		System.out.println("Number Added at Last!");
	}

	public void addAtIndex(int n, T num) {
		if (n < list.size() && n >= 0)
			list.add(n, num);
		else
			System.out.println("Index Invalid!");
	}

	public void removeAtFirst() {
		if (list.isEmpty()) {
			System.out.println("Stack is Empty!");
			return;
		}

		System.out.println("Removed Number:" + list.pop());
	}

	public void removeAtLast() {
		if (list.isEmpty()) {
			System.out.println("Stack is Empty!");
			return;
		}
		System.out.println("Removed Number:" + list.remove(list.size() - 1));
	}

	public void removeAtIndex(int n) {
		if (list.isEmpty() || n < 0 || n > list.size()) {
			System.out.println("Invalid Index!");
			return;
		}

		System.out.println("Removed Number:" + list.remove(n));
	}

	public void searchNumber(T num) {
		List<Integer> indexes = new ArrayList<Integer>();
		Iterator<T> it = list.listIterator();
		while (it.hasNext())
			if (it.next().equals(num)) {
				indexes.add(list.indexOf(it.next()));
			}

		if (indexes.isEmpty())
			System.out.println("Number not found");

		else {
			for (Integer i : indexes)
				System.out.println(i.intValue());
		}

	}
	
	public void print(){
		System.out.println(list);
	}



}
