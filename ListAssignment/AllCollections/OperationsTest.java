package AllCollections;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class OperationsTest {

	public static void printMenu() {
		System.out
				.println("Select an Operation to Perform: \n1.Add 10 numbers to list \n2.Add a number at the first position in the list \n3.Add a number at the last index position in a list \n4.Add a number N at position X in the list. \n5.Delete first number in the list. \n6.Delete last number in the list. \n7.Delete a number at position X in the list. Print the number that deleted from the list. \n8.Search for all the occurrences of a number X in the list.\n9.Exit\n10.Print Elements \nEnter Your Choice:");
	}

	public static void printListMenu() {
		System.out.println("Select the type of Collection:\n"
				+ "1.ArrayList\n2.LinkedList\n3.Vector\n4.Stack");
	}

	public static void main(String args[]) {
		Operations listOp;
		Scanner k = new Scanner(System.in);
		printListMenu();

		while (true) {
			switch (k.nextInt()) {

			case 1:
				listOp = new Operations(ListEnum.ArrayList);
				break;

			case 2:
				listOp = new Operations(ListEnum.LinkedList);
				break;

			case 3:
				listOp = new Operations(ListEnum.Vector);
				break;

			case 4:
				listOp = new Operations(ListEnum.Stack);
				break;

			default:
				System.out.println("Invalid Choice. Choose again!\n\n");
				printListMenu();
				continue;
			}
			break;
		}

		while (true) {
			printMenu();
			try {
				switch (k.nextInt()) {

				case 1:
					System.out.println("Enter 10 numbers to add:");
					for (int i = 0; i < 10; i++)
						listOp.addItems(k.nextInt());
					break;

				case 2:
					System.out.println("Enter the number to add:");
					listOp.addAtFirst(k.nextInt());
					break;

				case 3:
					System.out.println("Enter the number to add at last:");
					listOp.addAtLast(k.nextInt());
					break;

				case 4:
					System.out
							.println("Enter the position and number to add to list:");
					listOp.addAtIndex(k.nextInt(), k.nextInt());
					break;

				case 5:
					listOp.removeAtFirst();
					break;

				case 6:
					listOp.removeAtLast();
					break;

				case 7:
					System.out
							.println("Enter the position of the number to be removed:");
					listOp.removeAtIndex(k.nextInt());
					break;

				case 8:
					System.out.println("Enter the number to be searched:");
					listOp.searchNumber(k.nextInt());
					break;

				case 9:
					System.exit(0);

				case 10:
					listOp.print();
					break;

				default:
					System.out.println("Invalid Choice!");
					printMenu();
					continue;
				}
			}

			catch (InputMismatchException ie) {
				System.out.println("Please Enter an Integer only\n\n");
				printMenu();
				continue;
			}

		}

	}

}
