package AllCollections;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

public class Operations<T extends Number> {

	Operations(ListEnum listed) {
		
		switch(listed){
		
		case ArrayList: this.list=new ArrayList<>(); break;
		case LinkedList:this.list=new LinkedList<>(); break;
		case Vector: this.list=new Vector<>(); break;
		case Stack: this.list=new Stack<>(); break;
		}
		
	}

	List<T> list;

	public void addItems(T item) {
		list.add(item);
	}

	public void addAtFirst(T num) {
		list.add(0, num);
		System.out.println("Number Added!");
	}

	public void addAtLast(T num) {
		list.add(list.size() - 1, num);
		System.out.println("Number Added at Last!");
	}

	public void addAtIndex(int n, T num) {
		if (n < list.size() && n >= 0)
			list.add(n, num);
		else
			System.out.println("Index Invalid!");
	}

	public void removeAtFirst() {
		if (list.isEmpty()) {
			System.out.println("List is Empty!");
			return;
		}

		System.out.println("Removed Number:" + list.remove(0));
	}

	public void removeAtLast() {
		if (list.isEmpty()) {
			System.out.println("List is Empty!");
			return;
		}
		System.out.println("Removed Number:" + list.remove(list.size() - 1));
	}

	public void removeAtIndex(int n) {
		if (list.isEmpty() || n < 0 || n > list.size()) {
			System.out.println("Invalid Index!");
			return;
		}

		System.out.println("Removed Number:" + list.remove(n));
	}

	public void searchNumber(T num) {
		List<Integer> indexes = new ArrayList<Integer>();
		Iterator<T> it = list.iterator();
		while (it.hasNext())
			if (it.next().equals(num)) {
				indexes.add(list.indexOf(it.next()));
			}

		if (indexes.isEmpty())
			System.out.println("Number not found");

		else {
			for (Integer i : indexes)
				System.out.println(i.intValue());
		}

	}

	public void print() {
		System.out.println(list);
	}

}
