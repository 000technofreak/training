package Strings;

public class StringTest {
	StringTest(String name){
		this.name=name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
	String name;
	public static void main(String args[]){
		String s = "ABC";
		String s1 = s;
		String s2=s.concat("D");
		//s1="ABCD";
		System.out.println(s==s1?"true":"false");
		System.out.println(s+"\t"+s1+"\t"+s2);
		StringTest t1 = new StringTest("t1");
		StringTest t2=t1;
		System.out.println(t1==t2);
		t2.setName("t2");
		System.out.println(t1==t2);
		System.out.println(t2.getName()+"\t"+t1.getName());
	}
	
}
