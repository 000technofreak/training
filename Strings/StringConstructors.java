package Strings;

import java.io.UnsupportedEncodingException;

public class StringConstructors {

	
	public static void main(String args[]) throws UnsupportedEncodingException{
		
		char[] c={'a','b','v','z'};
		String s = new String(c);
		System.out.println(new String(c,1,2));
		
		byte[] b = {65,66,67,68,69,70};
		s= new String(b);
		System.out.println(s);
		System.out.println(new String(b,2,3,"UTF-16"));
		
		StringBuffer ss = new StringBuffer("Helo");
		String sss = new String(ss);
		System.out.println(ss+"\t"+sss);
		
	}
}
