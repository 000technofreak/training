package training;

public class HashTeest {

	public String s1 = "Hello";
	
	@Override
	public int hashCode(){
	
		int hash=0;
		try {
			hash = 33*this.getClass().hashCode()+ java.util.Random.class.newInstance().nextInt();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return hash;
	}
	
	public static void main(String args[]){
		HashTeest h1 = new HashTeest();
		System.out.println(h1.hashCode());
		HashTeest h2 = new HashTeest();
		System.out.println(h2.hashCode());
		h1 = new HashTeest();
		System.out.println(h1.hashCode());
		System.out.println(h1.s1.hashCode());
		System.out.println(h2.s1.hashCode());

	}
}
