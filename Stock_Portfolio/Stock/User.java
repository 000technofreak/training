package Stock;

import java.util.ArrayList;
import java.util.List;

public class User {

	private String id;
	private String name, email, password, gender;
	private int age;

	//List<Portfolio> user_portfolios; *Optional List*

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getGender() {
		return gender;
	}

	public int getAge() {
		return age;
	}

	/*void addPortfolio(Portfolio p) {
		if (user_portfolios == null)
			user_portfolios = new ArrayList<>();
		user_portfolios.add(p);

	}*/

	public User(String id, String name, String email, String pwd, String gender,
			int age) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = pwd;
		this.gender = gender;
		this.age = age;
	}

}
