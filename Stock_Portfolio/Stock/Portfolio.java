package Stock;

import java.util.ArrayList;
import java.util.List;

public class Portfolio {

	User user;

	List<Company> user_subscription = new ArrayList<>();

	public Portfolio(User u, List<Company> companies) {
		this.user = u;
		this.user_subscription = companies;
	}

	public Portfolio(User u, Company c) {
		this.user = u;
		user_subscription.add(c);
	}

	public void addCompany(Company c) {
		user_subscription.add(c);
	}
	
	public void print(){
		System.out.println("User: "+user.getName()+"\n");
		for (Company comp: user_subscription)
		   System.out.println(comp.toString()+"\n");
	}

}
