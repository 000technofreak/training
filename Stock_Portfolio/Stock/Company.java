package Stock;

public class Company {

	private String symbol, name, location;
	private long volume;
	private double cur_stock, previous_closed, trade_volume;

	public Company(String sym, String name, String loc, long vol, double cur_stock,
			double previous_closed, double trade_volume) {
		this.symbol = sym;
		this.name = name;
		this.location = loc;
		this.volume = vol;
		this.cur_stock = cur_stock;
		this.previous_closed = previous_closed;
		this.trade_volume = trade_volume;
	}

	public double getCur_stock() {
		return cur_stock;
	}

	public double getPrevious_closed() {
		return previous_closed;
	}

	public double getTrade_volume() {
		return trade_volume;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public long getVolume() {
		return volume;
	}
	
	public String toString(){
		return symbol+"\t"+previous_closed+"\t"+cur_stock+"\t"+trade_volume+"\t"+volume;
	}
}
