package Annotations;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestReflection {

	public static void main(String[] args) {

		TestMethods tm = new TestMethods();
        Method[] methods = tm.getClass().getDeclaredMethods();

		for (Method method : methods) {
			System.out.println("Executing Method:" + method.getName());
			 if(method.getParameterTypes().length == 2){
	            	try {
	                    System.out.println(method.getName()+": "+method.invoke(tm, 3,2));
	                    System.out.println("testNumber:"+"\""+method.getAnnotation(Test.class).number()+"\"");
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            }
	            
	            else{
	            	try {
	                    System.out.println(method.getName()+": "+method.invoke(tm, 0));
	                    System.out.println("testNumber"+"\""+method.getAnnotation(Test.class).number()+"\"");

	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            }
	            
	           
		}
	}
}
