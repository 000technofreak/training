package Annotations;

public class TestMethods {
	
	@Test(number = 1)
	public boolean testMax(int a, int b){
		
		System.out.println("In testMax Method");
		return a>b;
		
	}
	
	
	@Test(number = 2)
	public boolean testMin(int a, int b){
		
		System.out.println("In testMin Method");
		return a<b;
	}
	
	@Test(number = 3)
	public boolean testZero(int a){
		
		System.out.println("In testZero Method");
		return a==0;
	}

}
