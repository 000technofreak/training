package com.training.customer;

public class Customer {

	private String Name;
	private String Address;
	private String phone;
	
	
	
	public Customer(String name,String add, String ph){
		Name=name;
		Address=add;
		phone=ph;
	}
	
	public String toString(){
		return this.Name+", "+this.Address+","+this.phone;
	}
	
	public String getAddress(){
		return Address;
	}
	
	public String getName(){
		return Name;
	}
	
	public String getPhone(){
		return phone;
	}
}
