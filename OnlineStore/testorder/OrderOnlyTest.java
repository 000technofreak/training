package com.training.testorder;
import com.training.order.Order;

public class OrderOnlyTest {
	static Order o;
	static long oDNum = 394893;
	static String date = "31-07-2014";
	public static void OrderSetUp(){
		o = new Order(oDNum,date,CustomerTest.c.getAddress(),CustomerTest.c);
		
	}
	
	static void testCustomer(){
		OrderSetUp();
		assert(o.getOdNum()==(oDNum));
	}
	
	public static void main(String[] args){
		testCustomer();
	}

}
