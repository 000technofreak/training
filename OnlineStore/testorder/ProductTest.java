package com.training.testorder;
import com.training.product.Product;

public class ProductTest {
	static Product p,p1;
	static int code = 1;
	static String description = "Pen";
	static double price = 13999.00;
	static int qty = 10;
	public static void ProductSetUp(){
		p = new Product(code,description,price,qty);
		p1= new Product(2,"Pencil",3.0,6);
		
	}
	
	static void testProduct(){
		ProductSetUp();
		assert(p.getProductCode()==(code));
	}
	
	public static void main(String[] args){
		testProduct();
	}

}
