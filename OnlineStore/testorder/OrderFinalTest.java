package com.training.testorder;

import com.training.order.OrderItem;

public class OrderFinalTest {

	public static void main(String[] args) {
		CustomerTest.CustomerSetUp();
		ProductTest.ProductSetUp();
		OrderOnlyTest.OrderSetUp();
		OrderItemTest.OrderItemSetUp();
		OrderOnlyTest.o.print();
		System.out.println("\nOrdered Items:");
		for (OrderItem item : OrderOnlyTest.o.getItemList())
			item.getOrderItem();
	}
}
