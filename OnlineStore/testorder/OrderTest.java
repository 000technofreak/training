package com.training.testorder;
import com.training.customer.Customer;
import com.training.order.Order;
import com.training.order.OrderItem;
import com.training.product.Product;
public class OrderTest {

	public static void main(String[] args){
	Customer c = new Customer("Venkatesh","Maula Ali","9033030303");
	Product p1 = new Product(1,"Xiaomi Mi3",13999.00,10);
	Order order1 = new Order(001, "12-Jan-2014", c.getAddress(), c);
	order1.addItem(new OrderItem(p1,2));
	order1.print();
	}
	
}
