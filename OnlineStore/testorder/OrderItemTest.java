package com.training.testorder;
import com.training.order.OrderItem;

public class OrderItemTest {
	static OrderItem oi,oi2;
	static int qty=5;
		public static void OrderItemSetUp(){
		oi = new OrderItem(ProductTest.p,qty);
		oi2 = new OrderItem(ProductTest.p1,2);
		OrderOnlyTest.o.addItem(oi);
		OrderOnlyTest.o.addItem(oi2);

	}
	
	static void testCustomer(){
		OrderItemSetUp();
		assert(oi.getProduct()==ProductTest.p);
	}
	
	public static void main(String[] args){
		testCustomer();
	}

}
