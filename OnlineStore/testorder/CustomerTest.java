package com.training.testorder;

import com.training.customer.Customer;

public class CustomerTest {
	public static Customer c;
	static String Name = "Tenki";
	static String Address = "BhenCIL";
	static String phone = "9049034930";

	public static void CustomerSetUp() {
		c = new Customer(Name, Address, phone);
	}

	static void testCustomer() {
		CustomerSetUp();
		assert (c.getName().equals(Name));
	}

	public static void main(String[] args) {
		testCustomer();
	}

}
