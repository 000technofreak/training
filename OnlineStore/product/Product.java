package com.training.product;

public class Product {

	public Product(int pcode,String desc,double pr,int qty){
		this.ProductCode=pcode;
		this.description=desc;
		this.price=pr;
		this.qty=qty;
	}
	int ProductCode;
	String description;
	double price;
	int qty;
	boolean isAvailable(){
		return qty>0;
	}
	
	public String toString(){
		return ProductCode+": "+description+": "+price;
	}
	
	public int getProductCode(){
		return ProductCode;
	}
}
