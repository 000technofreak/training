package com.training.order;
import java.util.ArrayList;
import java.util.List;

import com.training.product.*;

public class OrderItem {

	//String odcode = OrderNumber;
	Product p;
	int qty;
	
	public OrderItem(Product pro,int qty){
		p=pro;
		this.qty=qty;
	}
	
	public void getOrderItem(){
		System.out.println(p.toString());
	}
	
	public Product getProduct(){
		return p;
	}
}
