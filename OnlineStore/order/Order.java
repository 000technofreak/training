package com.training.order;
import java.util.ArrayList;
import java.util.List;

import com.training.customer.*;
import com.training.product.*;

public class Order {

	long OrderNumber;
	String OrderDate;
	String ShippingAddress;
	Customer customer;
	List<OrderItem> orderItem;

	enum status{
		created(0,"Order Created"),ordered(1,"Ordered"),shipped(2,"Order Shipped"),cancelled(3,"Order Cancelled");
		private final String description;
		private final int code;
		String getDescription(){
			return description;
		}
		
		int getCode(){
			return code;
		}
		
		status(int code, String desc){
			this.code=code;
			this.description=desc;
		}
		
		public String toString(){
			return "Status "+code+": "+description;
		}
		
		
	}
	
	status st;
	public void print(){
		System.out.println("Customer Name \t Order Number \t Order Date \t Shipping Address \t Status");
		System.out.println("-------------\t ------------- \t ----------- \t -------------- \t ------");
		System.out.println(customer.getName()+"\t"+ OrderNumber+" \t "+OrderDate+" \t "+ShippingAddress+" \t"+st.toString());
	}
	
	
	public Order(long odnum, String odDate, String shipping,Customer cust){
		OrderDate=odDate;
		OrderNumber=odnum;
		ShippingAddress=shipping;
		customer=cust;
	}
	
	public void addItem(OrderItem oi){
		if(orderItem==null)
			orderItem = new ArrayList<OrderItem>();
		orderItem.add(oi);
		st=status.ordered;
	}
	
	public long getOdNum(){
		return OrderNumber;
	}
	
	public List<OrderItem> getItemList(){
		return orderItem;
	}
}
