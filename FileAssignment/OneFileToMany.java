package MultipleFileAssignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class OneFileToMany {

	public static void main(String[] args) {

		try {
			BufferedReader reader = new BufferedReader(new FileReader("inTheEnd.txt"));
			BufferedWriter writer1 = new BufferedWriter(new FileWriter("part1.txt"));
			BufferedWriter writer2 = new BufferedWriter(new FileWriter("part2.txt"));
			String line;
			int lineNumber=0;
			while((line=reader.readLine())!=null){
				if((++lineNumber)%2!=0){
					writer1.write(line);
					writer1.newLine();
				}
				else{
					writer2.write(line);
					writer2.newLine();
				}
			}
			reader.close();
			writer1.close();
			writer2.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
