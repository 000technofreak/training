package MultipleFileAssignment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class FilterStreamTest {
	
	public static void main(String... args){
		try {
			FilterInputStream reader = new BufferedInputStream(new FileInputStream(new File("part1.txt")));
			FilterOutputStream writer = new BufferedOutputStream(new FileOutputStream(new File("filtertest.txt")));
			byte[] b = new byte[1];
			byte[] space = " ".getBytes();
			while(reader.read(b)!=-1){
				if(Arrays.equals(b, space))
					writer.write("***".getBytes());
				else
					writer.write(b);
			}
			
			reader.close();
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
