package FileAssignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileCopyWrite {

	public static void main(String args[]) {

		try {
			BufferedReader fileReader = new BufferedReader(new FileReader("test.txt"));
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter("testCopy.txt"));
			String line;
			while((line = fileReader.readLine())!=null)
				fileWriter.write(line);
			fileReader.close();
			fileWriter.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("File Not Found!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
