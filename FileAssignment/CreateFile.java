package FileAssignment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CreateFile {
	
	public static void main(String[] args) {
        try {
          File file = new File("test.txt");
          BufferedWriter output = new BufferedWriter(new FileWriter(file));
          output.write("Venkatesh writes code");
          output.write("Correction: Venkatesh Writes beautiful code :p");
          output.close();
        } catch ( IOException e ) {
           e.printStackTrace();
        }
    }

}
