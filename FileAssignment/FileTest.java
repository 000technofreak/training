package FileAssignment;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileTest {
	
	public static void main(String[] args){
		
		try {
			BufferedReader fileReader = new BufferedReader(new FileReader("testCopy.txt"));
			String line;
			while((line=fileReader.readLine())!=null)
				System.out.println(line+"\n");
			fileReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
