public class Product {

	String code;
	String Name;
	String Desc;
	double price;
	long available;

	Product(String code, String Name, String desc, double price, long avail) {
		this.code = code;
		this.Name = Name;
		this.Desc = desc;
		this.price = price;
		this.available = avail;
	}

	public void placeOrder(int qty) {
		if (available >= qty) // if product is present upto the quantity
		{
			System.out.println("Order Placed Successfully!");
			available -= qty;
			System.out.println("Available: " + available);
		} else
			System.out.println("Item Out of Stock");
	}

	public boolean isAvailable() {
		return available > 0 ? true : false;
	}

	public static void main(String args[]) {
		Product pen = new Product("111", "Reynolds", "Blue Ball Pens", 10, 4);
		Product pencil = new Product("112", "Natraj", "Natraj HB Pencils", 2, 5);
		Product sharpner = new Product("113", "Natraj",
				"Natraj Pencil Sharpner", 2.50, 2);
		if (pen.isAvailable())
			pen.placeOrder(3);
		if (pencil.isAvailable())
			pencil.placeOrder(4);
		if (sharpner.isAvailable())
			sharpner.placeOrder(3);

	}
}
