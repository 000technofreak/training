import java.io.IOException;


public class Practice {


	public static void main(String args[]) throws IOException
	{
		for(int i=2;i<=100;i++)
			if(i%2==0)
			System.out.println(i);
		char c = (char) System.in.read();
		if (c=='k')
			System.out.println("Right");
		else
		{
			System.out.println("Wrong");
			if(c<'k')
				System.out.println("Too Low");
			else
				System.out.println("Too High");
		}
		
		
		for(int i=0;i<6;i++)
		{
			if(i==0)
				System.out.println("Value is zero");
			else if(i==1)
				System.out.println("Value is One");
			else if(i==2)
				System.out.println("Value is Two");
			else if(i==3)
				System.out.println("Value is Three");
			else if(i==4)
				System.out.println("Value is Four");
			else if(i==5)
				System.out.println("Value is Five");
			else
				System.out.println("Out of bound");
		}
		for(int i=0; i<6;i++) {
			switch(i)
			{
			case 0: System.out.println("Value is zero");
			        break;
			case 1: System.out.println("Value is one");
			        break;
			case 2: System.out.println("Value is two");
	                break;
			case 3: System.out.println("Value is three");
	                break;
			case 4: System.out.println("Value is four");
			        break;
			case 5: System.out.println("Value is five");
			        break;
			case 6: System.out.println("Value is six");
			        break;
			default: System.out.println("Value is illegal");
			         break;
			}
			
		}
		
	
	}
}

