
public class Cat extends Pet {

	String color;
	String description;
	
	Cat(String owner,int licence, boolean trained, String Color, String description)
	{
		super(owner,licence,trained);
		this.color=color;
		this.description=description;
	}
	
	public static void main(String args[])
	{
		Cat cat1 = new Cat("Venky",112,false,"black","Venky's Pet cat");
		System.out.println(cat1.isTrained()?"The cat is trained!":"The cat is not yet trained!");
	}
}
