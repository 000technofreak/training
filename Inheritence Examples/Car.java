
public class Car {

	Car(String Manufacturer, int seat, double price)
	{
		this.Manufacturer=Manufacturer;
		this.seating=seat;
		this.price=price;
	}
	static final int wheels = 4;
	String Manufacturer;
	int seating;
	double price;
	int gear=0;
	
	public void start()
	{
		System.out.println("Vroom Vroooom.. Engine Started");
	}
	
	public void changeGear()
	{
		gear++;
		System.out.println("Running in Gear:"+gear);
	}
	
	public void stop()
	{
		System.out.println("Breaks Applied! Engine Stopped. See ya Later!");
		System.exit(0);
	}
	
	public static void main(String args[])
	{
		Car AudiR8 = new Car("Audi",2,140000);
		AudiR8.start();
		AudiR8.changeGear();
		AudiR8.changeGear();
		AudiR8.changeGear();
		AudiR8.changeGear();
		AudiR8.changeGear();
		AudiR8.stop();
		
	}
	
}
