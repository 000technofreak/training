public class Rectangle {

	int x, y;
	double width, height;

	Rectangle(int x, int y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	Rectangle(double width, double height) {
		this(0, 0, width, height);
	}

	Rectangle() {
		this(0, 0, 1, 1);
	}

	public void print() {
		System.out.println("Coordinates: x="+x + "  y=" + y + "\nWidth=" + width + "\nHeight=" + height);
	}

	public static void main(String args[]) {
		Rectangle r = new Rectangle();
		r.print();
		Rectangle r1 = new Rectangle(23.3, 44.4);
		r1.print();
		Rectangle r2 = new Rectangle(3, 5, 55.5, 33.3);
		r2.print();
	}
}
