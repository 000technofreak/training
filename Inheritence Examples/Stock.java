public class Stock {

	Stock(String sym, double p, boolean list) {
		this.symbol = sym;
		this.price = p;
		this.listed = list;
	}

	String symbol;
	double price;
	boolean listed;

	public void updatePrice(double price) {
		this.price = price;
	}

	public boolean isListed() {
		return listed;
	}

	public static void main(String args[]) {
		Stock oracle = new Stock("ORCL", 44.55, false);
		System.out.println("Symbol: " + oracle.symbol + "\nPrice: "
				+ oracle.price);
		System.out.println(oracle.isListed() ? "Company is Listed"
				: "Not Listed");
		oracle.updatePrice(55.55);
		System.out.println("Updated Price:" + oracle.price);
	}
}
