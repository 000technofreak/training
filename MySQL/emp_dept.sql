create database testday2;
use testday2;

CREATE TABLE DEPARTMENT
(
   Dept_Num    int(10),
   Dept_Name   char(30),
   Location    varchar(33)
);

CREATE TABLE EMPLOYEE
(
   Emp_Num      int(4),
   Emp_Name     varchar(15),
   Job          varchar(45),
   Manager      char(4),
   Hire_Date    date,
   Salary       int(6),
   Commission   int(6),
   Dept_Num     int(2)
);


ALTER TABLE DEPARTMENT
ADD PRIMARY KEY (Dept_Num);

ALTER TABLE DEPARTMENT
CHANGE COLUMN Dept_Num Dept_Num INT(10) NOT NULL;

ALTER TABLE DEPARTMENT
CHANGE COLUMN Dept_Name Dept_Name char(30) UNIQUE;

ALTER TABLE DEPARTMENT
CHANGE COLUMN Location Location varchar(33) NOT NULL;

ALTER TABLE DEPARTMENT
CHANGE COLUMN Dept_Name Dept_Name varchar(15) UNIQUE;

ALTER TABLE EMPLOYEE
ADD PRIMARY KEY (Emp_Num);

ALTER TABLE EMPLOYEE
CHANGE COLUMN Emp_Num Emp_Num int(4) NOT NULL;

ALTER TABLE EMPLOYEE
ADD FOREIGN KEY (Dept_Num)
REFERENCES DEPARTMENT(Dept_Num);

ALTER TABLE EMPLOYEE
CHANGE COLUMN Salary Salary DECIMAL(6,2);

ALTER TABLE EMPLOYEE
ADD COLUMN DOB date
AFTER Emp_Name;

ALTER TABLE EMPLOYEE
DROP COLUMN DOB;

INSERT INTO DEPARTMENT VALUES (10, 'Accounting', 'ECIL'),
						      (20,'Constructing','Panjagutta'),
							  (30, 'Software', 'Madhapur'),
							  (40,'Education', 'DilsukhNagar');
							  
INSERT INTO EMPLOYEE  
VALUES (7369, 'SMITH', 'CLERK', 7902, '1980:12:17', 2800,0,20),
       (7499, 'ALLEN', 'SALESMAN', 7698, '1981:02:20', 1600, 300,30),    
       (7566, 'JONES', 'MANAGER', 7839, '1981:04:02', 3570,0,20),
	   (7654, 'MARTIN', 'SALESMAN', 7698, '1981:09:28', 1250, 1400, 30),
	   (7782, 'CLARK', 'MANAGER', 7839, '1981:06:09', 2940,0,10),
	   (7788, 'SCOTT', 'ANALYST', 7566, '1982:12:09', 3000,0,20),
	   (7839, 'KING', 'PRESIDENT', 7566, '1981:11:17', 5000,0, 10),
	   (7844, 'TURNER', 'SALESMAN', 7698, '1981:09:08', 1500,0,30),
	   (7876, 'ADAMS', 'CLERK', 7788, '1983:01:12',3100,0,20),
	   (7900, 'JAMES', 'CLERK', 7698, '1981:12:03', 2950,0, 30),
	   (7902, 'FORD', 'ANAYLYST', 7566, '1981:12:03', 3000,0, 10),
	   (7934, 'MILLER', 'CLERK', 7782, '1982:01:23', 3300,0,20),
	   (7591, 'WARD', 'SALESMAN', 7698, '1981:02:22', 500,0,30),
	   (7698, 'BLAKE', 'MANAGER', 7839, '1981:05:01', 3420, 0,30),
	   (7777, 'MILL', 'ANALYST', 7839, '1981:05:01', 2000, 200, NULL);
	   
	   
SELECT * FROM department;

SELECT dept_num, location FROM department;

SELECT * FROM employee;

SELECT emp_num,
       emp_name,
       hire_date,
       dept_num
  FROM employee;






