USE testday2;

#Views:
/* Create view consisting of department number with more than 5 employees in each department.
Create view to show all the columns from EMPLOYEE Table with their corresponding department name and location.*/
CREATE VIEW dept_view1 AS
SELECT Dept_Num FROM EMPLOYEE
GROUP BY Dept_Num HAVING COUNT(Dept_Num) > 5;

CREATE VIEW dept_view2 AS
SELECT E.*, D.Dept_Name, D.Location FROM EMPLOYEE E, DEPARTMENT D
WHERE E.Dept_Num = D.Dept_Num;

#Sql Functions:
#Display employee number and total salary for each employee.
SELECT Emp_Num, SUM(Commission+Salary) AS TOTALSALARY FROM EMPLOYEE;

#Display maximum salary from employee table.
SELECT Emp_Name, MAX(Salary) FROM EMPLOYEE;

#Display minimum salary from employee table.
SELECT Emp_Name, MIN(Salary) FROM EMPLOYEE;

#Display average salary from employee table.
SELECT AVG(Salary) AS AVGSALARY FROM EMPLOYEE;

#Display maximum salary being paid to CLERK.
SELECT MAX(Salary) FROM EMPLOYEE
WHERE Job = 'CLERK';

#Display total salary drawn by an analyst working in dept no 20
SELECT SUM(Salary+Commission) AS TOTALSALARY FROM EMPLOYEE
WHERE Job = 'ANALYST' AND Dept_Num = 20;

#Display department numbers and total number of employees working for each department.
SELECT Dept_Num, COUNT(Emp_Num) AS TOTALEMPLOYEE FROM EMPLOYEE
GROUP BY Dept_Num;

#Display the various jobs and total number of employees working in each department.
SELECT Job, COUNT(Emp_Num) AS TOTALEMPLOYEE FROM EMPLOYEE
GROUP BY Dept_Num;

#Display department numbers and total salary of each department.
SELECT Dept_Num, SUM(Commission+Salary) AS TOTALSALARY
FROM EMPLOYEE
GROUP BY Dept_Num;

#Display department numbers and maximum salary in each department.
SELECT Dept_Num, MAX(Salary) AS MAXSALARY
FROM EMPLOYEE
GROUP BY Dept_Num;

#Display the names of salesman who earns a salary more than a highest salary of the clerk.
SELECT Emp_Name FROM EMPLOYEE
WHERE Job = 'SALESMAN' AND Salary > (SELECT MAX(Salary) FROM EMPLOYEE
                                     WHERE Job = 'CLERK');
									 
#Compute average, minimum and maximum salaries of the group of employees having the job of CLERK or MANAGER.
SELECT AVG(Salary) AS AVG_SALARY, MIN(Salary) AS MINSALARY, MAX(Salary) AS MAXSALARY
FROM EMPLOYEE WHERE Job = 'CLERK';

#Display the names of employees who earn highest salary in their respective departments.
SELECT Emp_Name,MAX(Salary) AS HIGHEST_SALARY FROM EMPLOYEE
GROUP BY Dept_Num;

#Display job groups having total salary greater than the maximum salary of managers.
SELECT Job, SUM(Salary) FROM EMPLOYEE 
GROUP BY Salary HAVING SUM(Salary) > (SELECT MAX(Salary) FROM EMPLOYEE WHERE Job = 'MANAGER');

#Answer floor, ceil and round value for '111.99' value.
SELECT FLOOR(111.9), CEIL(111.99), ROUND(111.99);

#Display employee name in Upper case
SELECT UPPER(Emp_Name) FROM EMPLOYEE;

#Display the length of each department name.
SELECT Dept_Name, CHAR_LENGTH(Dept_Name) AS LENGTH FROM DEPARTMENT;

#Display the current date
SELECT CURDATE() AS CURRENT_DATE;

#Display the date after 3 months from current date
SELECT DATE_ADD(CURDATE(), INTERVAL 3 MONTH);

#Display the last working day for the current month.
#SELECT IF(DAYNAME(LAST_DAY(CURDATE()))<>'Saturday' AND DAYNAME(LAST_DAY(CURDATE()))<>'Sunday', DAYNAME(LAST_DAY(CURDATE())), 'Its a weekend');
SELECT CASE WHEN DAYNAME(LAST_DAY(CURDATE()))='Saturday' THEN DAYNAME(DATE_SUB(LAST_DAY(CURDATE()), INTERVAL  DAY)) ELSE 'WEEKEND';

#Display current date in 'dd-mm-yyyy' format and 'dd-mm-yy'.
SELECT DATE_FORMAT(CURDATE(),'%d-%m-%Y');
SELECT DATE_FORMAT(CURDATE(),'%d-%m-%y');

#For an employee commission is null put a '0'.
UPDATE EMPLOYEE
SET Commission = 0
WHERE Commisssion = NULL;

#Display all distinct jobs for employees.
SELECT DISTINCT Job FROM EMPLOYEE;

#Display first 3 letters of employee name.
SELECT LEFT(Emp_Name,3) FROM EMPLOYEE;

#Assignment Joins and Indexes:

#Get all department numbers that are present in DEPT Table , but not in EMP Table.
SELECT DISTINCT E.Dept_Num FROM EMPLOYEE E
LEFT JOIN DEPARTMENT D ON D.Dept_Num <> E.Dept_Num;

#List all columns from both Department and employee.
SELECT * FROM EMPLOYEE E
INNER JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num;

#Perform equi join on tables Employee and Department
SELECT * FROM EMPLOYEE E, DEPARTMENT D
WHERE D.Dept_Num = E.Dept_Num;

#Perform self join on tables Employee and Department
SELECT e.Emp_Name AS 'Employee',
       m.Emp_Name AS 'reports to'    
FROM employee e
INNER JOIN employee m ON m.Emp_Num = e.Manager
ORDER BY m.Manager;

#Perform inner join on tables Employee and Department
SELECT * FROM EMPLOYEE E
INNER JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num;

#Perform left outer  join on tables Employee and Department
SELECT DISTINCT * FROM EMPLOYEE E
LEFT OUTER JOIN DEPARTMENT D ON D.Dept_Num = E.Dept_Num;

#Perform right outer join on tables Employee and Department
SELECT DISTINCT * FROM EMPLOYEE E
RIGHT OUTER JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num;

#Perform full outer join on tables Employee and Department
SELECT * FROM EMPLOYEE E
LEFT JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num
UNION
SELECT * FROM EMPLOYEE E
RIGHT JOIN DEPARTMENT D ON E.Dept_Num = D.Dept_Num;


#Assignment Sub-queries:

#Display the managers whose salary is more than the average salary of his employees. 
SELECT Emp_Name FROM EMPLOYEE E
WHERE Job = 'Manager' AND Salary > (SELECT AVG(Salary) FROM EMPLOYEE D WHERE E.Emp_Num = D.Manager);

#Display the name, hire date, job and department name from employee and department tables whose dept no is 10
SELECT E.Emp_Name, E.Hire_Date, E.Job, D.Dept_Name FROM EMPLOYEE E, DEPARTMENT D
WHERE D.Dept_Name = (SELECT Dept_Name FROM DEPARTMENT WHERE Dept_Num = 10);

#Display name, hire date, job and department name from employee and department tables whose dept no is 20
SELECT E.Emp_Name, E.Hire_Date, E.Job, D.Dept_Name FROM EMPLOYEE E, DEPARTMENT D
WHERE D.Dept_Name = (SELECT Dept_Name FROM DEPARTMENT WHERE Dept_Num = 10);

#Display the name of employees that do not belong to any department. 
SELECT E.Emp_Name FROM EMPLOYEE E
WHERE E.Dept_Num NOT IN (SELECT Dept_Num FROM DEPARTMENT);

#Display those who worked as managers using correlated sub query.
SELECT Emp_Name, Job FROM EMPLOYEE
WHERE Emp_Num = ANY (SELECT Emp_Num FROM EMPLOYEE WHERE Job = 'MANAGER');


