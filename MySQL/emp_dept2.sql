
use testday2;

SELECT * FROM EMPLOYEE
WHERE Job = 'SALESMAN';

SELECT * FROM EMPLOYEE
WHERE year(Hire_Date) >= 1982;

SELECT Emp_Num, Emp_Name, Job, Hire_Date, Salary FROM EMPLOYEE
WHERE Job = 'MANAGER';

SELECT * FROM EMPLOYEE
WHERE Commission > Salary;

SELECT * , YEAR(CURDATE()) - YEAR(Hire_Date) AS Experience FROM EMPLOYEE
WHERE Salary/30 > 200;

SELECT * FROM EMPLOYEE
WHERE Commission = 0;

SELECT Emp_Name, YEAR(CURDATE()) - YEAR(Hire_Date) AS Experience
FROM EMPLOYEE;

SELECT * FROM EMPLOYEE
WHERE Salary+(Salary*0.20) > 3000;

SELECT Emp_Name FROM EMPLOYEE
WHERE YEAR(CURDATE()) - YEAR(Hire_Date) = 32;

SELECT * FROM EMPLOYEE
WHERE Salary > 2000;

SELECT Emp_Num, Emp_Name, Job, Hire_Date FROM EMPLOYEE
WHERE Dept_Num = (SELECT Dept_Num FROM DEPARTMENT WHERE Dept_Name = 'Accounting');

SELECT Emp_Name, Salary FROM EMPLOYEE
WHERE Job = 'CLERK';

SELECT 