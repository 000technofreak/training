
CREATE TABLE user
(
   id         int PRIMARY KEY,
   name       varchar(45),
   email      varchar(45),
   password   varchar(45),
   Gender     char(6),
   age        int
);

CREATE TABLE company
(
   symbol     varchar(10) PRIMARY KEY,
   name       varchar(45),
   volume     bigint,
   Location   varchar(45)
);

CREATE TABLE portfolio
(
   id        int PRIMARY KEY,
   name      varchar(45),
   user_id   int,
   FOREIGN KEY(user_id) REFERENCES user(id) ON UPDATE CASCADE
);

CREATE TABLE stockinfo
(
   id                int PRIMARY KEY,
   cur_date_time     datetime,
   current_stock     double,
   previous_closed   double,
   trade_volume      bigint,
   company_symbol    varchar(10),
   FOREIGN KEY
      (company_symbol)
       REFERENCES company(symbol) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE portfolio_contains_company
(
   portfolio_id     int PRIMARY KEY,
   company_symbol   varchar(10),
   FOREIGN KEY
      (portfolio_id)
       REFERENCES portfolio(id) ON UPDATE CASCADE ON DELETE RESTRICT,
   FOREIGN KEY
      (company_symbol)
       REFERENCES company(symbol) ON UPDATE CASCADE ON DELETE RESTRICT
);
