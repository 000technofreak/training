use training;
CREATE TABLE DEPT (
       dept_code varchar(10) primary key,
	   Name varchar(100)
	   );
	   
CREATE TABLE EMPLOYEE (
       EmpID int primary key,
	   Fname varchar(50),
	   LName varchar(50),
	   EmpNumber varchar(50) NOT NULL UNIQUE,
	   dept_code varchar(10) NULL,
	   FOREIGN KEY (dept_code)
       REFERENCES EMPLOYEE(dept_code)
       ON UPDATE CASCADE ON DELETE RESTRICT
	   );
	  
INSERT INTO DEPT
     VALUES ('DC001', 'Civil Engg');

INSERT INTO DEPT
     VALUES ('DC002', 'Mech Engg');

INSERT INTO DEPT
     VALUES ('DC003', 'Computer Science Engg');

INSERT INTO EMPLOYEE
     VALUES (1,
             'Ramaswamy',
             'Tem',
             'E001',
             'DC001');

INSERT INTO EMPLOYEE
     VALUES (2,
             'Rama',
             'Sam',
             'E002',
             'DC002');

INSERT INTO EMPLOYEE
     VALUES (3,
             'Chanda',
             'Mouli',
             'E003',
             'DC003');

INSERT INTO EMPLOYEE
     VALUES (4,
             'Srinivas',
             'Kota',
             'E004',
             'DC001');

INSERT INTO EMPLOYEE
     VALUES (5,
             'Ravi',
             'Babu',
             'E005',
             'DC002');

INSERT INTO EMPLOYEE
     VALUES (6,
             'Anand',
             'Hoy',
             'E006',
             'DC003');
