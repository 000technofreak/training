package WaterBottlingPlant;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Truck extends Thread {

	private DataInputStream waterBottles;
	int bottlesCapacity = 75;

	Truck(InputStream is) {
		waterBottles = new DataInputStream(is);
	}

	@Override
	public void run() {

		System.out.println("Truck is ready to be loaded!");
		try {
			System.out.println("Truck is being filled...");
			while (true) {
				if (waterBottles.readInt() == bottlesCapacity) {
					System.out.println("Truck Filled! Leaving for warehouse!");
					this.run();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
