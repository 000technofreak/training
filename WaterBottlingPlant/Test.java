package WaterBottlingPlant;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Test {

	public static void main(String... ar) throws IOException {
		
		int BUFFER = 75;
		PipedInputStream pis = new PipedInputStream(BUFFER);
		PipedOutputStream pos = new PipedOutputStream(pis);
		Pump p = new Pump(pos);
		Truck t = new Truck(pis);
		p.start();
		t.start();	
	}

}
