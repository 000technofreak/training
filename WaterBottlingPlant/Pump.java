package WaterBottlingPlant;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Pump extends Thread {

	DataOutputStream filler;

	Pump(OutputStream out) {
		filler = new DataOutputStream(out);
	}

	@Override
	public void run() {
		
		int bottlesToFill = 400;
		System.out.println("Pump is filling truck..");
		for (int i = 1; i <= bottlesToFill; i++) {
			try {
				filler.writeInt(i);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(i==bottlesToFill){
					System.out.println("Pump has finished filling...");
					//this.sleep(1000);
					this.run();
				}  
			
		}
	
	}
}
