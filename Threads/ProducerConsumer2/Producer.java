package ProducerConsumer2;

import java.util.ArrayList;

public class Producer extends Thread {

	int count=0;
	ArrayList<Bucket> buckets;
	String name;
	Producer(ArrayList<Bucket> b, String name) {
		this.buckets = b;
		this.name=name;
	}

	@Override
	public void run() {
		 for (Bucket bucket : buckets) {
		      while (bucket.count < bucket.capacity) {
		        bucket.addBall();
		        System.out.println(this.name + "new Count in " + bucket.name + " is " + bucket.count);
		      }
		      System.out.println("Bucket Full " + bucket.name);
		    }
		    System.out.println("Exit from producer " + this.name);
		    return;
		  }
}
