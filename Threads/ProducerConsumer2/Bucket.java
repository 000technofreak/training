package ProducerConsumer2;

public class Bucket {

	int count=0,capacity;
	String name;
	Bucket(String name,int capacity){
		this.capacity=capacity;
		this.name=name;
	}
	
	public void addBall(){
		if(++count==capacity)
			{System.out.println("Bucket "+name+" is Full!"); return;}
		System.out.println("Ball "+count+" Added!");
	}
	
	public boolean removeBall(){
		if(--count>0)
			{return true;}
		return false;
	}
}
