package ProducerConsumer2;

import java.util.ArrayList;

public class ProducerConsumerTest {

	public static void main(String args[]) {
		Bucket b = new Bucket("B1",100);
		Bucket b2 = new Bucket("B2",100);
		ArrayList<Bucket> list = new ArrayList<Bucket>();
		list.add(b);
		list.add(b2);
		Producer p1 = new Producer(list,"p1");
		p1.start();
		Consumer c1 = new Consumer(list);
	}

}
