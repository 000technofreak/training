package ProducerConsumer2;

import java.util.ArrayList;

public class Consumer extends Thread {

	ArrayList<Bucket> buckets;

	Consumer(ArrayList<Bucket> b) {
		this.buckets = b;
		this.start();
	}

	@Override
	public void run() {
		for(Bucket bucket: buckets){
		      bucket.removeBall();
			System.out.println("Ball "+bucket.count+" Removed!");
			if (bucket.count<=1){
				System.out.println("Bucket Empty!");
				try {
					this.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		notifyAll();
	}

}
