package BankingApp;

public class AccountOps extends Thread{
	
	Account account;

	AccountOps(Account acc, String name){
		super.setName(name);
		account=acc;
		this.start();
	}
	
	@Override
	public void run(){
		
		synchronized(account){
			System.out.println("Running Thread: "+super.getName());
			account.addBalance(10000);
			account.withdraw(1000);
			account.changeName("Hello");
		}
	}

}
