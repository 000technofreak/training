package BankingApp;

public class Account {
	
	double balance;
	String name;
	int number;
	
	Account(double balance, String name, int num){
		
		this.balance=balance;
		this.name=name;
		this.number=num;
	}
	
	public synchronized void updateBalance(double newbal){
		balance=newbal;
		System.out.println("New Balance: "+balance);
	}
	
	public synchronized void addBalance(double amt){
		System.out.println("Updating Balance...");
		updateBalance(balance+amt);
	}
	
	public synchronized void withdraw(double amt){
		if(amt<=balance){
		  System.out.println("Withdrawing Amount..");
		  updateBalance(balance-amt);}
		else
			System.out.println("Invalid Amount!");
	}
	
	public synchronized void changeName(String newname){
		name=newname;
		System.out.println("Name Changed to: "+name);
	}

}
