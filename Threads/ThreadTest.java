public class ThreadTest extends Thread {

	String name;

	public ThreadTest(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		for (int i = 0; i < 1000; i++) {
			System.out.println(this.name + "Hello World " + i);
			try{
				this.sleep(1000);
			}
			
			catch(InterruptedException ie){
				ie.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {
		ThreadTest ThreadTest1 = new ThreadTest("Thread1");
		ThreadTest ThreadTest2 = new ThreadTest("Thread2");
		ThreadTest ThreadTest3 = new ThreadTest("Thread3");

		ThreadTest1.start();
		ThreadTest2.start();
		ThreadTest3.start();
	}
}
