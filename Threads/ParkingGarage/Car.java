package ParkingGarage;

public class Car extends Thread {

	Garage garage;
	String name;
	int state = 0;

	Car(Garage g, String n) {
		garage = g;
		name = n;
		this.start();
	}

	@Override
	public void run() {
		while (true) {
			if(state==0){
			garage.enter();
			state = 1;
			try {
				this.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			garage.leave();
			state = 0;
		}
		
		
		//System.out.println(this.getState());
	}

}
