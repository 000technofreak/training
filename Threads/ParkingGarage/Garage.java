package ParkingGarage;

public class Garage {
	
	private int places=0,capacity;
	
		Garage(int capacity){
		this.capacity=capacity;
	}
	
	public synchronized boolean enter(){
		if(places<capacity){
			places++;
			System.out.println("Car parked at "+places);
			return true;
		}
		
		System.out.println("Garage Full! No place to park!");
		return false;
	}

	
	public synchronized boolean leave(){
		if(places<=0){
			System.out.println("Garage is Empty!");
			return false;
		}
		System.out.println("Car left the garage at "+(places--));
		return true;
	}
	
	
}
