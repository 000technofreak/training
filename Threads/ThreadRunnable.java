public class ThreadRunnable implements Runnable {

	String name;

	public ThreadRunnable(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}

	public void run() {

		for (int i = 0; i < 1000; i++) {
			System.out.println(this.name + " Hello World " + i);
		}

	}

	public static void main(String[] args) {
		ThreadRunnable myThread1 = new ThreadRunnable("RunnableThread1");
		ThreadRunnable myThread2 = new ThreadRunnable("RunnableThread2");
		ThreadRunnable myThread3 = new ThreadRunnable("RunnableThread3");

		Thread t1 = new Thread(myThread1);
		Thread t2 = new Thread(myThread2);
		Thread t3 = new Thread(myThread3);

		t1.start();
		t2.start();
		t3.start();

	}
}
