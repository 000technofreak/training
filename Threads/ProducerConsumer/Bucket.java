package ProducerConsumer;

public class Bucket {

	int count=0,capacity;
	
	Bucket(int capacity){
		this.capacity=capacity;
	}
	
	public void addBall(){
		if(++count==capacity)
			{System.out.println("Bucket is Full!"); return;}
		System.out.println("Ball "+count+" Added!");
	}
	
	public boolean removeBall(){
		if(--count>0)
			{return true;}
		return false;
	}
}
