package ProducerConsumer;

public class Producer extends Thread {

	int count=0;
	Bucket b;

	Producer(Bucket b) {
		this.b = b;
		this.start();
	}

	@Override
	public void run() {
		while (count<=b.capacity) {
			b.addBall();
			count++;
			if (count == b.capacity){
				try {
					this.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		notifyAll();
	}

}
