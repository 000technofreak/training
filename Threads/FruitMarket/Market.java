package FruitMarket;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Market {

	int capacity;
	int freeStalls;
	BlockingQueue<Farmer> farmers;
	Customer consumer;

	public Market(int capacity, int spotsOccupied) {
		this.capacity = capacity;
		this.freeStalls = spotsOccupied;
		farmers = new LinkedBlockingQueue<>(capacity);

	}

	public void useStall(Farmer farmer) {

		addFarmer(farmer);

		freeStalls++;
		System.out.println("Spot no. " + freeStalls
				+ " has been occupied by " + farmer.getName());

	}

	public void enterMarket(Customer consumer) {
		this.consumer = consumer;
	}

	public void releaseStall(Farmer farmer) {

		try {
			System.out.println("Spot no. " + freeStalls
					+ " has been released by " + farmers.take().getName());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		freeStalls--;

	}

	private synchronized void addFarmer(Farmer farmer) {
		try {
			farmers.put(farmer);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
