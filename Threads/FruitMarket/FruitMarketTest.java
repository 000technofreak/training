package FruitMarket;

public class FruitMarketTest {
	
	public static void main(String[] args) {
		
		Market market=new Market(4,0);

		Farmer farmer=new Farmer(10, 10, 10, 10, "Farmer1",market);
		Farmer farmer1=new Farmer(7, 9, 11, 13, "Farmer2",market);
		Farmer farmer2=new Farmer(8, 10, 12, 14, "Farmer3",market);
		Farmer farmer3=new Farmer(6, 8, 10, 12, "Farmer4",market);
	
		Customer customer1=new Customer(10, 10, 10, 10,"Cusotmer1",market);
		Customer customer2=new Customer(5, 5, 5, 5,"Customer2",market);
		Customer customer3=new Customer(8, 7, 9, 9,"Customer3",market);
		Customer customer4 = new Customer(6,8,2,1,"Customer4",market);
		
		farmer.start();
		farmer1.start();
		farmer2.start();
		farmer3.start();
		customer1.start();
		customer2.start();
		customer3.start();
		
	}


}
