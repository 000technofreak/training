package FruitMarket;

import java.util.ArrayList;
import java.util.List;

public class Farmer extends Thread {

	List<Integer> fruits = new ArrayList<>();

	public synchronized List<Integer> getFruits() {
		return fruits;
	}

	String name;

	Market market;

	public Farmer(int apple, int orange, int grape, int watermelon,
			String name, Market market) {

		super(name);
		fruits.add(apple);
		fruits.add(orange);
		fruits.add(grape);
		fruits.add(watermelon);
		this.market = market;

	}

	public void run() {

		market.useStall(this);
		long start = System.currentTimeMillis();
		while (!allEmpty(fruits)) {

			try {
				sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (System.currentTimeMillis() - start > 5000) {
				if (allEmpty(fruits))
					System.out.println(this.getName()
							+ " is leaving market, couldnt sell all fruits");
				else
					System.out.println(this.getName()
							+ " is leaving market, sold off all fruits");
				break;
			}
		}
		market.releaseStall(this);

	}

	boolean allEmpty(List<Integer> list) {
		boolean empty = true;
		for (Integer i : list) {
			if (i != 0)
				empty = false;
		}

		return empty;
	}
}