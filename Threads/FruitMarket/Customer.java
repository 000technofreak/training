package FruitMarket;

import java.util.ArrayList;
import java.util.List;

public class Customer extends Thread {

	List<Integer> grocery = new ArrayList<>();

	String name;
	Market market;

	public Customer(int apple, int orange, int grape, int watermelon,
			String name, Market market) {

		super(name);
		grocery.add(apple);
		grocery.add(orange);
		grocery.add(grape);
		grocery.add(watermelon);
		this.market = market;
	}

	public void run() {
		buy();
	}

	public synchronized void buy() {
		System.out.println();
		while (!(allEmpty(grocery) && market.farmers.isEmpty())) {

			for(Farmer farmer : market.farmers) {

				List<Integer> basket = farmer.getFruits();
				for (int i = 0; i < grocery.size(); i++) {

					int customerReq = grocery.get(i), fruitCount = basket
							.get(i);

					if (customerReq != 0)
						if (customerReq == fruitCount) {
							basket.set(i, 0);
							grocery.set(i, 0);
							System.out.println(this.getName() + " bought "
									+ customerReq + " fruits from "
									+ farmer.getName());
						} else if (customerReq < fruitCount) {
							basket.set(i, fruitCount - customerReq);
							grocery.set(i, 0);
							System.out.println(this.getName() + " bought "
									+ customerReq + " fruits from "
									+ farmer.getName());
						} else {
							grocery.set(i, customerReq - fruitCount);
							basket.set(i, 0);
							System.out.println(this.getName() + " bought "
									+ customerReq + " fruits from "
									+ farmer.getName());
						}

				}

				if (!allEmpty(grocery))
					try {
						System.out.println("Market ran out of stock. "
								+ this.getName() + " is waiting........");
						this.wait(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					break;
			}
		}

		if (allEmpty(grocery))
			System.out.println(this.getName()
					+ " is leaving market, he got what he wanted.");
		else
			System.out.println(this.getName()
					+ " is leaving market,couldnt find fruits.");

	}

	boolean allEmpty(List<Integer> list) {
		boolean empty = true;
		for (Integer i : list) {
			if (i != 0)
				empty = false;
		}

		return empty;
	}

}
