package MultipleProducerConsumer;

public class Bucket {
	String name;

	int count=0,capacity;
	
	Bucket(int capacity, String name){
		this.name = name;
		this.capacity=capacity;
	}
	
	public String getName() {
		return name;
	}
	
	public void addBall(){
		if(++count==capacity)
			{System.out.println("Bucket is Full!" + this.name); return;}
		System.out.println("Ball "+count+" Added!");
	}
	
	public boolean removeBall(){
		if(--count>0)
			{return true;}
		return false;
	}
}
