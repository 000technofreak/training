package MultipleProducerConsumer;

public class Consumer extends Thread {

	Bucket b;

	Consumer(Bucket b) {
		this.b = b;
		this.start();
	}

	@Override
	public void run() {
		while (b.removeBall()) {
			System.out.println("Ball "+b.count+" Removed!");
			if (b.count<=1){
				System.out.println("Bucket Empty!");
				try {
					this.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		notifyAll();
	}

}
