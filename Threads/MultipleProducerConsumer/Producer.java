package MultipleProducerConsumer;

public class Producer extends Thread {

	int count = 0;
	Bucket b;

	Producer(Bucket b) {
		this.b = b;
		//this.start();
	}

	@Override
	public void run() {
		while (b.count < b.capacity) {
			b.addBall();
			if (b.count == b.capacity) {
				try {
					System.out.println("Waiting..");
					this.wait();
					System.out.print(this.getState());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		notifyAll();

	}

	public boolean addBucket(Bucket b) {

		// System.out.println(this.getState());

		if (this.b.count < this.b.capacity) {
			System.out.println("capacity not reached..");
			return false;
		}
		System.out.println("capacity reached..");

		// if (this.getState() == Thread.State.BLOCKED) {

		this.b = b;
		this.start();
		return true;
		// }
		// return false;

	}

}
