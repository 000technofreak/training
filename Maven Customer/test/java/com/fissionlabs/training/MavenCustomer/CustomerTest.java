package com.fissionlabs.training.MavenCustomer;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import com.fissionlabs.training.MavenCustomer.dao.CustomerDAO;
import com.fissionlabs.training.MavenCustomer.model.Customer;

public class CustomerTest {
	CustomerDAO cusDAO;
	Customer cust;

	@Before
	public void setUp() throws Exception {
		cusDAO = new CustomerDAO();
	}

	@Test
	public void testCustomer() {
		try {
			cust = cusDAO.getCUSTOMER(10);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println(cust.toString());
	}

}
