package com.fissionlabs.training.MavenCustomer.connection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Singleton Pattern... One and only one factory to create many Connection objects

public class ConnectionFactory implements DBHelper {

  // static reference to itself
  private static ConnectionFactory instance = new ConnectionFactory();

  // private constructor
  private ConnectionFactory() {
    /*try {
      Class.forName(DRIVER_CLASS);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
*/
  }

  private Properties getProperties() throws PropertiesNotLoaded, PropertiesConfigNotFound {
    Properties props = new Properties();
    String CONFIG_ABS_PATH = this.getClass().getResource(CONFIG_FILE).getPath();
    System.out.println(CONFIG_ABS_PATH);

    FileInputStream fis;

    try {
      fis = new FileInputStream(new File(CONFIG_ABS_PATH));
    } catch (FileNotFoundException e) {
      throw new PropertiesConfigNotFound(CONFIG_ABS_PATH);
    }

    try {
      props.load(fis);
    } catch (IOException e) {
      e.printStackTrace();
      throw new PropertiesNotLoaded(CONFIG_ABS_PATH);
    }

    return props;
  }

  private Connection createConnection() {

    Connection connection = null;
    try {
      connection = DriverManager.getConnection(DB_URL, getProperties());
    } catch (SQLException e) {
      System.out.println("ERROR: Unable to Connect to Database.");
      
    } catch (PropertiesConfigNotFound e) {
      e.printStackTrace();
    } catch (PropertiesNotLoaded e) {
      e.printStackTrace();
    }
    return connection;
  }

  public static Connection getConnection() {
    return instance.createConnection();
  }
}

class PropertiesConfigNotFound extends FileNotFoundException {
  private static final long serialVersionUID = 8611923216256772573L;

  public PropertiesConfigNotFound(String message) {
    super("Properties file not found " + message);
  }
}

class PropertiesNotLoaded extends IOException {
  private static final long serialVersionUID = -4621447436794088987L;

  public PropertiesNotLoaded(String message) {
    super("Properties from " + message + " could not be loaded");
  }
}