package com.fissionlabs.training.MavenCustomer.model;


// Transfer Object (TO) pattern
public class Customer {
  private int id;
  private String name;
  public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getPhone() {
	return phone;
}

public void setPhone(String phone) {
	this.phone = phone;
}

private String address;
  private String phone;

  public Customer() {

  }

  public Customer(int id, String name, String address, String phone) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.phone = phone;
  }

 
  @Override
  public boolean equals(Object obj) {
    if (obj == null || obj.getClass() != getClass())
      return false;

    Customer customer = (Customer) obj;
    if (this.id == customer.getId() && this.name.equals(customer.getName()))
      return true;
    else
      return false;

  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 7 * hash + this.id;
    hash = 7 * hash + this.name.hashCode();
    return hash;
  }
  
  @Override
  public String toString(){
	  return "Id: "+this.id+"\t Name: "+this.name+"\t Address: "+this.address+"\t Phone: "+this.phone;
  }

}
