package com.fissionlabs.training.MavenCustomer.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.fissionlabs.training.MavenCustomer.connection.ConnectionFactory;
import com.fissionlabs.training.MavenCustomer.connection.DBUtil;
import com.fissionlabs.training.MavenCustomer.model.Customer;

// DAO or Data Access Object design pattern, to encapsulate the access to the database
public class CustomerDAO {
  private static final String DROP_CUSTOMER = "drop table CUSTOMER";
  private static final String CREATE_CUSTOMER = "create table CUSTOMER (customerId int AUTO_INCREMENT Primary Key, customerName varchar(30),customerAddress varchar(50), customerPhone varchar(10))";
  private static final String INSERT_CUSTOMER = "insert into CUSTOMER(customerId, customerName, customerAddress, customerPhone) values(10,'Venkatesh','Maula Ali','9000000')";
  private static final String UPDATE_CUSTOMER = "update CUSTOMER SET customerPhone='8787878' where customerID=";
  private static final String DELETE_CUSTOMER = "delete from CUSTOMER where customerId=";
  private static final String SELECT_CUSTOMER = "SELECT * FROM CUSTOMER WHERE customerId=";

  public CustomerDAO() {
  }


  
  public Customer getCUSTOMER(int CUSTOMERId) throws SQLException {
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;
    
    String query = SELECT_CUSTOMER + CUSTOMERId;
    Customer CUSTOMER = null;
    try {
      connection = ConnectionFactory.getConnection();
      statement = connection.createStatement();
      rs = statement.executeQuery(query);

      while(rs.next())
        CUSTOMER = new Customer(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
    } finally {
      DBUtil.close(rs);
      DBUtil.close(statement);
      DBUtil.close(connection);
    }
    return CUSTOMER;
  }
  private int executeCUSTOMERQuery(String query) throws SQLException {
    Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    int success = 0;
    try {
      con = ConnectionFactory.getConnection();
      st = con.createStatement();
      success = st.executeUpdate(query);
    } finally {
      DBUtil.close(rs);
      DBUtil.close(st);
      DBUtil.close(con);
    }
    
    return success;
  }
  
  public void createCUSTOMER() throws SQLException, ClassNotFoundException {
   int c = executeCUSTOMERQuery(CREATE_CUSTOMER);
   System.out.println("Table Created " + c);
  }
  
  public void dropCUSTOMER() throws SQLException, ClassNotFoundException {
    int c = executeCUSTOMERQuery(DROP_CUSTOMER);
    System.out.println("Table Dropped " + c);
   }
  

  public void addCUSTOMER() throws SQLException, ClassNotFoundException {
    Connection con = ConnectionFactory.getConnection();
    Statement st = con.createStatement();
    int r = st.executeUpdate(INSERT_CUSTOMER);
    System.out.println("no. of rows inserted : " + r);
  }

  /*
   * public void addCUSTOMERs() throws SQLException, ClassNotFoundException {
   * Connection con = ConnectionFactory.getConnection(); Statement st =
   * con.createStatement(); int r =
   * st.executeUpdate("insert into T_ADDRESS values(10,'2-1-58','hyderabad')");
   * System.out.println("no. of rows inserted : " + r); }
   * 
   * public void updateCUSTOMER() throws SQLException, ClassNotFoundException {
   * Connection con = ConnectionFactory.getConnection(); Statement st =
   * con.createStatement(); int r =
   * st.executeUpdate("insert into T_ADDRESS values(10,'2-1-58','hyderabad')");
   * System.out.println("no. of rows inserted : " + r); }
   * 
   * public void deleteCUSTOMER() throws SQLException, ClassNotFoundException {
   * Connection con = ConnectionFactory.getConnection(); Statement st =
   * con.createStatement(); int r =
   * st.executeUpdate("insert into T_ADDRESS values(10,'2-1-58','hyderabad')");
   * System.out.println("no. of rows inserted : " + r); }
   */
}
