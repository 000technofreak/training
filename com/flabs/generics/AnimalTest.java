package com.flabs.generics;

class BoundGen<T extends Bird>{
	T bird;
	BoundGen(T animal){
		bird = animal;
	}
	
	void showType(){
		System.out.println(bird.getClass().getName());
	}
	
	T getBird(){
		return bird;
	}
}


class Animal{
	
}

class AnimalTest {

	public static void main(String[] args){
		//BoundGen<Animal> ga = new BoundGen<Animal>(new Animal());
		BoundGen<Parrot> gg = new BoundGen<Parrot>(new Parrot());
		gg.showType();
	}
}
