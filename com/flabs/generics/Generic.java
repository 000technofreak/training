package com.flabs.generics;

public class Generic<T> {

	private T bird;

	Generic(T b) {
		this.bird = b;
	}

	T getBird() {
		return bird;
	}

	void showType() {
		System.out.println(bird.getClass().getName());
	}

	public static void main(String... args) {

		Generic<Parrot> g = new Generic<Parrot>(new Parrot("Parrot"));
		g.showType();
		Generic<Duck> g1 = new Generic<Duck>(new Duck("Duck"));
		g1.showType();
		Parrot p = g.getBird();
		Duck d = g1.getBird();
		System.out.println(p.type);
		// Parrot p1 = (Parrot) ng1.getBird();

		System.out.println(p.toString());
		System.out.println(p.hashCode() + " " + new Parrot().hashCode());
		System.out.println(d.hashCode());

	}

}

class Bird {
	String type;

	Bird() {
		System.out.println("In Bird");
	}

	void flying() {
		System.out.println("FLying");
	}

	void makeSound() {
		System.out.println("Bird Sound");
	}
}

class Parrot extends Bird implements flyable {
	Parrot() {
		System.out.println("In Parrot");
	}

	Parrot(String s) {
		type = s;
	}

	@Override
	void makeSound() {
		System.out.println("Can speak");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Flyable Parrot");
	}

	@Override
	public int hashCode() {
		return 41 * this.getClass().getName().hashCode();
	}

}

class Duck extends Bird implements flyable, swimmable {
	Duck() {
		System.out.println("In DUck");
	}

	public Duck(String string) {
		// TODO Auto-generated constructor stub
		type = string;
	}

	void makeSound() {
		System.out.println("Quack");
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Duck Swim");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Duck FLy");
	}
	
	@Override
	public int hashCode() {
		return 41 * this.getClass().getName().hashCode();
	}
}

interface flyable {
	void fly();
}

interface swimmable {
	void swim();
}
