package com.flabs.generics;

public class TwoGen<T,T1> {

		private T bird; private T1 bird1;
		TwoGen(T b, T1 bi){
			this.bird = b;
			this.bird1=bi;
		}
		
		T getBird(){
			return bird;
		}
		
		TwoGen<T,T1> getTwoBird(){
		
			return this;
		}
		
		void showType(){
			System.out.println(bird.getClass().getName());
			System.out.println(bird1.getClass().getName());

		}


		public static void main(String[] args){
			
			TwoGen<Parrot,Duck> g = new TwoGen<Parrot,Duck>(new Parrot("Parrot"),new Duck("Duck"));
			g.showType();
			TwoGen twoBirds = g.getTwoBird();
			//twoBirds.showType();
		}
		
	}

	class Bird{
		String type;
		Bird(){
			System.out.println("In Bird");
		}
		
		void flying(){
			System.out.println("FLying");
		}
		
		void makeSound(){
			System.out.println("Bird Sound");
		}
	}
	class Parrot extends Bird implements flyable{
		Parrot(){
			System.out.println("In Parrot");
		}
		
		Parrot(String s){
			type = s;
		}
		
		@Override
		void makeSound(){
			System.out.println("Can speak");
		}

		@Override
		public void fly() {
			// TODO Auto-generated method stub
			System.out.println("Flyable Parrot");
		}
		
	}

	class Duck extends Bird implements flyable,swimmable{
		Duck(){
			System.out.println("In DUck");
		}
		
		public Duck(String string) {
			// TODO Auto-generated constructor stub
			type = string;
		}

		void makeSound(){
			System.out.println("Quack");
		}

		@Override
		public void swim() {
			// TODO Auto-generated method stub
			System.out.println("Duck Swim");
		}

		@Override
		public void fly() {
			// TODO Auto-generated method stub
			System.out.println("Duck FLy");
		}
	}

	interface flyable{
		void fly();
	}

	interface swimmable{
		void swim();
	}



