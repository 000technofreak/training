package com.flabs.generics;

public class Stats<T extends Number> {
	T[] nums;
	Stats(T[] array){
		nums = array;
	}
	double getAverage(){
		double sum=0;
		for (T i:nums)
			sum+=i.doubleValue();
		return sum/nums.length;
	}
	
	boolean sameAverage(Stats<? extends Number> num){
		return num.getAverage()==this.getAverage();
	}

	
	public static void main(String args[]){
		Integer[] nums = {10,20,30,40,50};
		Double[] n = {10.0,20.0,30.0,40.0,50.0};
		Stats<Integer> s = new Stats<Integer>(nums);
		System.out.println(s.getAverage());
		Stats<Double> sd = new Stats<Double>(n);
		System.out.println(sd.getAverage());
		System.out.println(sd.sameAverage(s)); //true
	}
}
