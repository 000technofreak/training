package com.flabs.generics;

public class NonGenerics {
	private Bird bird;
	NonGenerics(Bird b){
		this.bird = b;
	}
	
	Bird getBird(){
		return bird;
	}
	
	void showType(){
		System.out.println(bird.getClass().getName());
	}

		
	public static void main(String... args){
		
		NonGenerics ng = new NonGenerics(new Parrot("Parrot"));
		ng.showType();
		NonGenerics ng1 = new NonGenerics(new Duck("Duck"));
		ng1.showType();
		Parrot p = (Parrot) ng.getBird();
		Duck d = (Duck) ng1.getBird();
		System.out.println(p.type);
		//Parrot p1 = (Parrot) ng1.getBird();
	
	}
	
}

class Bird{
	String type;
	Bird(){
		System.out.println("In Bird");
	}
	
	void flying(){
		System.out.println("FLying");
	}
	
	void makeSound(){
		System.out.println("Bird Sound");
	}
}
class Parrot extends Bird implements flyable{
	Parrot(){
		System.out.println("In Parrot");
	}
	
	Parrot(String s){
		type = s;
	}
	
	@Override
	void makeSound(){
		System.out.println("Can speak");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Flyable Parrot");
	}
	
}

class Duck extends Bird implements flyable,swimmable{
	Duck(){
		System.out.println("In DUck");
	}
	
	public Duck(String string) {
		// TODO Auto-generated constructor stub
		type = string;
	}

	void makeSound(){
		System.out.println("Quack");
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Duck Swim");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Duck FLy");
	}
}

interface flyable{
	void fly();
}

interface swimmable{
	void swim();
}