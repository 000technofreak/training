package com.flabs.collections;

import java.util.Collection;
import java.util.EnumMap;

public class EnumMapTest {

	public enum Numbers {

		ONE, TWO, THREE, FOUR, FIVE
	};

	public static void main(String[] args) {
		EnumMap<Numbers, String> map = new EnumMap<Numbers, String>(
				Numbers.class);
		
		
		map.put(Numbers.ONE, "1");
		map.put(Numbers.TWO, "2");
		map.put(Numbers.THREE, "3");
		map.put(Numbers.FOUR, "4");

		
		System.out.println("Map: " + map);

		Collection<String> values = map.values();

		System.out.println("Collection:" + values);
	}

}
