package com.flabs.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetTest {
	
	 public static void main(String[] args) {

			HashSet<Integer> hashSet = new HashSet<Integer>();
			for (int i = 1; i <= 40; i++) {
			    hashSet.add(i);
			}

			Iterator<Integer> iterator = hashSet.iterator();
			while (iterator.hasNext()) {
			    System.out.print(iterator.next() + ",");
			}

			System.out.println();

			LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<Integer>();
			for (int i = 1; i <= 40; i++) {
			    linkedHashSet.add(i);
			}
			
			Iterator<Integer> it = linkedHashSet.iterator();
			while (it.hasNext()) {
			    System.out.print(it.next() + ",");
			}
		    }

}
