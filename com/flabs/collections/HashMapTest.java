package com.flabs.collections;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HashMapTest {

	public static void main(String args[]) {

		Map<String, String> addr = new HashMap<>();

		addr.put("Venkatesh", "Maulali");
		addr.put("Ritu", "Miyapur");
		addr.put("Sanchit", "Jubilee Hills");
		addr.put("Fission", "Jubilee Hills");

		System.out.println("Key Value Pair:\n" + addr);

		if (addr.containsKey("Venkatesh"))
			System.out.println("Venkatesh stays at " + addr.get("Venkatesh"));

		Set<String> s = addr.keySet();
		for (String key : s)
			System.out.println(key + " stays at " + addr.get(key));

		for (Map.Entry<String, String> entry : addr.entrySet())
			System.out
					.println(entry.getKey() + " stays at " + entry.getValue());

		System.out.println(addr.put("Venkatesh", "Bowenpally"));

		TreeMap<String,String> tm = new TreeMap<>(addr); //assign map to treemap

		System.out.println(tm); //key-value pairs are in sorted alphabetical order of key
		
		LinkedHashMap<String, String> lhm = new LinkedHashMap<>(addr); 
		
		System.out.println(lhm); //prints the order of insertion
	}

}
