package com.flabs.collections;

import java.util.Enumeration;
import java.util.Vector;

class Person {
	String name = "";
	String address = "B. Hills";

	public Person(String name) {
		this.name = name;
	}

	public void setAddr(String addr) {
		this.address = addr;
	}
}

public class EnumerationTest {
	public static void main(String[] args) {
		Vector<Person> v = new Vector<Person>();
		v.add(new Person("a"));
		v.add(new Person("b"));
		v.add(new Person("c"));
		System.out.println("elements in vector : " + v);
		System.out.println(v.get(0).name + ", " + v.get(0).address);
		System.out.println(v.get(1).name + ", " + v.get(1).address);
		System.out.println(v.get(2).name + ", " + v.get(2).address);
		Enumeration<Person> e = v.elements();
		while (e.hasMoreElements()) {
			Person st = e.nextElement();
			if (st.name.equals("b")) {
				st.setAddr("hyderabad");
			}
		}
		System.out.println("elements after changing");
		System.out.println(v.get(0).name + ", " + v.get(0).address);
		System.out.println(v.get(1).name + ", " + v.get(1).address);
		System.out.println(v.get(2).name + ", " + v.get(2).address);
	}

}
