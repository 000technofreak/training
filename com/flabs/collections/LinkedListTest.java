package com.flabs.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LinkedListTest {
	
	public static void doTimings(String type, List<Integer> list){
		long start = System.currentTimeMillis();
		
		for(int i=0;i<1E5;i++)
			list.add(0,i);
		
		long end = System.currentTimeMillis();
		
		System.out.println("Time taken for "+type+" : "+(end-start)+" ms");
	}
	
	public static void main(String[] args){
	List<Integer> arrayList = new ArrayList<>();
	List<Integer> linkedList = new LinkedList<>();
	doTimings("ArrayList",arrayList);
	doTimings("LinkedList", linkedList);
	}
}
