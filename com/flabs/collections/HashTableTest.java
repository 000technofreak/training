package com.flabs.collections;

import java.util.Hashtable;

public class HashTableTest {

	public static void main(String[] args){
		Hashtable ht = new Hashtable(3,5.0f);
		
		ht.put(1, "one");
		ht.put(2, "two");
		ht.put(3, "three");
		
		System.out.println(ht.size());
		ht.put(4, "four");
		
		System.out.println(ht+"\n"+ht.size());
		
		ht.put(5,"five");
		
		System.out.println(ht.size());
		
		
		
	}
}
