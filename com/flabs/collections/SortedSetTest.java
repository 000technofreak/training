package com.flabs.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetTest {

	public static void main(String[] args) {

		Persons p0 = new Persons("Jim", 5.8);
		Persons p1 = new Persons("Mike", 6.2);
		Persons p2 = new Persons("Jack", 6.2);
		Persons p3 = new Persons("Jim", 5.9);

		// equals with compareTo method
		System.out.println("equals with compareTo() : "
			+ (p1.compareTo(p2) == 0));
		// equals with equals method
		System.out.println("equals with equals() : " + p1.equals(p2));

		SortedSet<Persons> sortedSet = new TreeSet<Persons>();
		sortedSet.add(p0);
		sortedSet.add(p1);
		sortedSet.add(p2);
		sortedSet.add(p3);

		System.out.println("elements in TreeSet : " + sortedSet);

		Set<Persons> set = new HashSet<Persons>();
		set.add(p0);
		set.add(p1);
		set.add(p2);
		set.add(p3);

		System.out.println("elements in hashset : " + set);
	    }
	}

	class Persons implements Comparable<Persons> {
	    private String name;
	    private double height;

	    Persons(String name, double height) {
		this.name = name;
		this.height = height;
	    }

	    public double getHeight() {
		return this.height;
	    }

	    @Override
	    public int compareTo(Persons o) {
		if (this.height == o.getHeight())
		    return 0;
		else if (this.height > o.height)
		    return 1;
		else
		    return -1;
	    }

	    @Override
	    public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.name).append("..").append(this.height);
		return sb.toString();
	    }
    
}