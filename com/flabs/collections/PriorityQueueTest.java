package com.flabs.collections;

import java.util.NoSuchElementException;
import java.util.PriorityQueue;

public class PriorityQueueTest {

	public static void main(String[] args) {

	    PriorityQueue<Integer> pQ = new PriorityQueue<Integer>();

	    System.out.println("Initial size of the priority queue is : " + pQ.size());
	    try {
	      System.out.println("Removing an element from empty Queue : ");
	      System.out.println(pQ.remove());
	    } catch (NoSuchElementException ne) {
	      System.out.println(ne + " can't perform remove when the PriorityQueue is empty");
	    }

	    // peek and element do not remove elements
	    System.out.println("Element returned by peek : " + pQ.peek());

	    try {
	      System.out.println("Element returned by element() : " + pQ.element());
	    } catch (NoSuchElementException ne) {
	      System.out.println(ne + " Cannot perform element() when PriorityQueue is empty");
	    }

	    System.out.println("Element removed by poll : " + pQ.poll());

	    System.out.println(pQ.add(20));

	    try {
	      System.out.println(pQ.offer(null));
	    } catch (NullPointerException ce) {
	      System.out.println(ce + " Can't add null to a PriorityQueue");
	    }
	  }
}
