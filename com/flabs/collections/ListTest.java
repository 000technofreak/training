package com.flabs.collections;

import java.util.*;

public class ListTest {

		
	public static void main(String[] args){
		Bird b = new Duck();
		List<Bird> birds = new ArrayList<Bird>();
		birds.add(new Parrot());
		birds.add(new Duck());
		birds.add(new Bird());
		birds.add(b);
		birds.add(b);
		List<Bird> birds1 = new ArrayList<Bird>(birds);
		for(Bird bird: birds1)
		   bird.makeSound();
		System.out.println(birds.containsAll(birds1));
		birds1.add(new Duck());
		System.out.println(birds.containsAll(birds1));
	
 }
}

class Bird{
	Bird(){
		System.out.println("In Bird");
	}
	
	void flying(){
		System.out.println("FLying");
	}
	
	void makeSound(){
		System.out.println("Bird Sound");
	}
}
class Parrot extends Bird implements flyable{
	Parrot(){
		System.out.println("In Parrot");
	}
	
	@Override
	void makeSound(){
		System.out.println("Can speak");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Flyable Parrot");
	}
	
}

class Duck extends Bird implements flyable,swimmable{
	Duck(){
		System.out.println("In DUck");
	}
	
	void makeSound(){
		System.out.println("Quack");
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Duck Swim");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Duck FLy");
	}
}

interface flyable{
	void fly();
}

interface swimmable{
	void swim();
}