package com.flabs.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

	class ComparableComparator implements Comparable<ComparableComparator> {
		private int id;
		private String name;
		private int salary;
		private int age;
		private Date dateOfJoining;

		public static final Comparator<ComparableComparator> AgeComparator = new Comparator<ComparableComparator>() {

			@Override
			public int compare(ComparableComparator o1, ComparableComparator o2) {
				return o1.age - o2.age; // This will work because age is
										// positive integer
			}

		};

		public static final Comparator<ComparableComparator> SalaryComparator = new Comparator<ComparableComparator>() {

			@Override
			public int compare(ComparableComparator o1, ComparableComparator o2) {
				return o1.salary - o2.salary; // salary is also positive integer
			}

		};

		public static final Comparator<ComparableComparator> NameComparator = new Comparator<ComparableComparator>() {

			@Override
			public int compare(ComparableComparator o1, ComparableComparator o2) {
				return o1.name.compareTo(o2.name);
			}

		};

		public static final Comparator<ComparableComparator> DOJComparator = new Comparator<ComparableComparator>() {

			@Override
			public int compare(ComparableComparator o1, ComparableComparator o2) {
				return o1.dateOfJoining.compareTo(o2.dateOfJoining);
			}

		};

		public ComparableComparator(int id, String name, int salary, int age,
				Date dateOfJoining) {
			this.id = id;
			this.name = name;
			this.salary = salary;
			this.age = age;
			this.dateOfJoining = dateOfJoining;
		}

		@Override
		public String toString() {
			return "SortedSetEmployee{" + "id=" + id + ", name=" + name + ", salary="
					+ salary + ", age=" + age + ", dateOfJoining="
					+ dateOfJoining + '}';

		}

		@Override
		public int compareTo(ComparableComparator o) {
			return this.id - o.id;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final ComparableComparator other = (ComparableComparator) obj;
			if (this.id != other.id) {
				return false;
			}
			if ((this.name == null) ? (other.name != null) : !this.name
					.equals(other.name)) {
				return false;
			}
			if (this.salary != other.salary) {
				return false;
			}
			if (this.age != other.age) {
				return false;
			}
			if (this.dateOfJoining != other.dateOfJoining
					&& (this.dateOfJoining == null || !this.dateOfJoining
							.equals(other.dateOfJoining))) {

				return false;
			}
			return true;
		}

		@Override
		public int hashCode() {
			int hash = 5;
			hash = 47 * hash + this.id;
			hash = 47 * hash + (this.name != null ? this.name.hashCode() : 0);
			hash = 47 * hash + this.salary;
			hash = 47 * hash + this.age;
			hash = 47
					* hash
					+ (this.dateOfJoining != null ? this.dateOfJoining
							.hashCode() : 0);
			return hash;
		}


	public static void main(String args[]){
		ComparableComparator e1 = new ComparableComparator(1, "A", 1000, 32, new Date(2011, 10, 1));
		ComparableComparator e2 = new ComparableComparator(2, "AB", 1300, 22, new Date(2012, 10, 1));
		ComparableComparator e3 = new ComparableComparator(3, "B", 10, 42, new Date(2010, 11, 3));
		ComparableComparator e4 = new ComparableComparator(4, "CD", 100, 23, new Date(2011, 10, 1));
		ComparableComparator e5 = new ComparableComparator(5, "AAA", 1200, 26, new Date(2011, 10, 1));

		List<ComparableComparator> listOfSortedSetEmployees = new ArrayList<ComparableComparator>();
		listOfSortedSetEmployees.add(e1);
		listOfSortedSetEmployees.add(e2);
		listOfSortedSetEmployees.add(e3);
		listOfSortedSetEmployees.add(e4);
		listOfSortedSetEmployees.add(e5);
		System.out.println("Unsorted List : " + listOfSortedSetEmployees);

		Collections.sort(listOfSortedSetEmployees); // Sorting by natural order
		System.out.println(e1.equals(listOfSortedSetEmployees.get(0)));
		System.out.println(e5.equals(listOfSortedSetEmployees.get(4)));

		Collections.sort(listOfSortedSetEmployees, ComparableComparator.NameComparator);
		System.out.println(e1.equals(listOfSortedSetEmployees.get(0)));
		System.out.println(e4.equals(listOfSortedSetEmployees.get(4)));

		Collections.sort(listOfSortedSetEmployees, ComparableComparator.AgeComparator);
		System.out.println(e2.equals(listOfSortedSetEmployees.get(0)));
		System.out.println(e3.equals(listOfSortedSetEmployees.get(4)));

		Collections.sort(listOfSortedSetEmployees, ComparableComparator.SalaryComparator);
		System.out.println(e3.equals(listOfSortedSetEmployees.get(0)));
		System.out.println(e2.equals(listOfSortedSetEmployees.get(4)));

		Collections.sort(listOfSortedSetEmployees, ComparableComparator.DOJComparator);
		System.out.println(e3.equals(listOfSortedSetEmployees.get(0)));
		System.out.println(e2.equals(listOfSortedSetEmployees.get(4)));
	}

}
