package com.flabs.collections;

import java.util.HashSet;
import java.util.Set;

public class SetImplementation {
	
	 public static void main(String[] args) {

		 
			Set<String> s = new HashSet<>();
			boolean a = s.add("A");
			boolean b = s.add("B");
			boolean c = s.add("B");
			System.out.println("Added A : " + a);
			System.out.println("Added B : " + b);
			System.out.println("Added B second time : " + c);

			System.out.println(s);

			boolean n = s.add(null);
			boolean n2 = s.add(null);
			System.out.println("first null : " + n);
			System.out.println("second null : " + n2);

			System.out.println(s);
		    }

}
