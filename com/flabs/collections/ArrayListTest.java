package com.flabs.collections;

import java.util.ArrayList;
import java.util.Iterator;


public class ArrayListTest {
	
	
	public static void main(String[] args){
	
	ArrayList<Integer> al = new ArrayList<>();
	for(int i=0;i<1000;i++)
		al.add(i+1);
	Object[] i = al.toArray();
	Integer[] io = new Integer[10];
	Integer[] im = al.toArray(io);
	System.out.println(io.length+"\t"+im.length);
	al.trimToSize();
	System.out.println(	al.size()); //1000
	
	ArrayList<Integer> al2 = new ArrayList<>(100);
	al2.add(23);
	al2.add(42);
	System.out.println(al2.size()); //2
	
	
	Iterator<Integer> it = al.iterator();
	while(it.hasNext())
		System.out.print(it.next()+" ");
	}

}
