package com.flabs.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetTest {
	public static void main(String[] args){
		Bird b = new Duck();
		Set<Bird> birds = new HashSet<Bird>();
		birds.add(new Parrot());
		birds.add(new Duck());
		birds.add(new Bird());
		birds.add(b);
		System.out.println(birds.add(b)); //false
		//System.out.println(birds.add(null));
		//System.out.println(birds.add(null)); //false
		Set<Bird> birds1 = new HashSet<Bird>(birds);
		/*for(Bird bird: birds1)
		   bird.makeSound(); //nullpointerexception*/
		Iterator<Bird> i = birds.iterator();
		while(i.hasNext())
		i.next().makeSound();
		System.out.println(birds.containsAll(birds1));
		birds1.add(new Duck());
		System.out.println(birds.containsAll(birds1));
	}
}

class Bird{
	Bird(){
		System.out.println("In Bird");
	}
	
	void flying(){
		System.out.println("FLying");
	}
	
	void makeSound(){
		System.out.println("Bird Sound");
	}
}
class Parrot extends Bird implements flyable{
	Parrot(){
		System.out.println("In Parrot");
	}
	
	@Override
	void makeSound(){
		System.out.println("Can speak");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Flyable Parrot");
	}
	
}

class Duck extends Bird implements flyable,swimmable{
	Duck(){
		System.out.println("In DUck");
	}
	
	void makeSound(){
		System.out.println("Quack");
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Duck Swim");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Duck FLy");
	}
}

interface flyable{
	void fly();
}

interface swimmable{
	void swim();
}