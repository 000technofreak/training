package com.flabs.collections;

import java.util.Stack;

public class StackTest {
	
	public static void main(String[] args) {

		Stack<Integer> stack = new Stack<Integer>();
		System.out.println("after creating, stack is empty : " + stack.empty());
		// push() ---Returns the 1-based position where an object is on this
		// stack.
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(2);
		stack.push(7);

		System.out.println("stack elements : " + stack);
		// peek() ---Returns the 1-based position where an object is on this
		// stack.
		Object last = stack.peek();
		System.out.println("box.peek() returns  " + last);
		System.out.println("elements of Box are " + stack);

		// pop() ---Removes the object at the top of this stack and returns that
		// object as the value of this function.
		Object removed = stack.pop();
		System.out.println("removed element from stack : " + removed);
		System.out
				.println("elements of Stack after pop() method is called are "
						+ stack);

		// search() ----Removes the object at the top of this stack and returns
		// that object as the value of this function.
		System.out.println("Position of 2 using search method is "
				+ stack.search(2));

		System.out.println("empty() returns true if stack is empty  "
				+ stack.empty());
		
		stack.add(4, 1);
		System.out.println(stack);

	}

}
