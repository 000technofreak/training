package com.flabs.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetTest {

	public static void main(String args[]) {
		Set<Integer> hSet = new HashSet<>();

		Integer[] intArray = new Integer[10];

		intArray[0] = new Integer(11);
		intArray[1] = new Integer(22);
		intArray[2] = new Integer(3);
		intArray[3] = new Integer(3);
		intArray[4] = new Integer(7);
		intArray[5] = new Integer(17);
		intArray[6] = new Integer(6);
		intArray[7] = new Integer(13);
		intArray[8] = new Integer(0);
		intArray[9] = new Integer(1);

		/* Adding Elements */
		hSet.add(intArray[0]);
		hSet.add(intArray[1]);
		hSet.add(intArray[2]);
		hSet.add(intArray[3]);
		hSet.add(intArray[4]);
		hSet.add(intArray[5]);
		hSet.add(intArray[6]);
		hSet.add(intArray[7]);
		hSet.add(intArray[8]);
		hSet.add(intArray[9]);

		System.out.println("Elements Before Sorting:");

		for (Integer intVal : hSet) {
			System.out.println(intVal.intValue());
		}

		/* Creating TreeSet passing HashSet instance in its constructor */
		Set<Integer> tSet = new TreeSet<>(hSet);

		System.out.println("Elements After Sorting:");

		for (Integer intVal : tSet) {
			System.out.println(intVal.intValue());
		}
	}

}
