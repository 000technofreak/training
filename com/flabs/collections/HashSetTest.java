package com.flabs.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class HashSetTest {

	public static void main(String args[]){
	ArrayList<Integer> ali = new ArrayList<Integer>();

	for (int i = 0; i < 10; i++) {
	    ali.add(i);
	    ali.add(i);
	}

	// adding duplicate elements to the array list

	System.out.println("elements in arraylist \n" + ali+"\t Size of arraylist: "+ali.size());

	// removing the duplicates using HashSet constructor
	Set<Integer> set1 = new HashSet<Integer>(ali);
	System.out.println(set1.size());
	System.out.println("elements in set :\n" + set1);

	// removing duplicates using the add method
	Set<Integer> set2 = new HashSet<Integer>();
	for (int i = 0; i < ali.size(); i++) {
	    set2.add(ali.get(i));
	}
	System.out.println("elements in set2 :\n" + set2);
    }
}
