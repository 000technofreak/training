package com.flabs.collections;

import java.util.Iterator;
import java.util.Vector;

public class IteratorTest {
	
	public static void main(String[] args) {
		Vector<Integer> v = new Vector<Integer>();
		v.add(10);
		v.add(20);
		v.add(30);
		v.add(40);

		System.out.println("elements in vector : " + v);
		Iterator<Integer> it = v.iterator();

		System.out.println("retreiving elements using iterator");
		while (it.hasNext()) {
			System.out.print(it.next() + ", ");
		}
		
		it = v.iterator();
		while (it.hasNext()) {

			if (it.next().equals(20)) {
				System.out.println("removing element using iterator");
				it.remove();
			}
		}
		System.out.println("Elements in the vector " + v);
		
		System.out.println("fast fail -- concurrent modification exception");
		v.add(50);
		System.out.println(it.next());
	}

}
