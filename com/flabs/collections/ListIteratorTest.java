package com.flabs.collections;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ListIteratorTest {
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("B");
		list.add("C");
		list.add("D");

		System.out.println("elements in list : " + list);

		ListIterator<String> listIterator = list.listIterator();

		System.out.println("printing from first -");
		while (listIterator.hasNext()) {
			System.out.println("index " + listIterator.nextIndex() + " : "
					+ listIterator.next());
		}

		System.out.println("printing from last -");
		while (listIterator.hasPrevious()) {
			System.out.println("index " + listIterator.previousIndex() + " : "
					+ listIterator.previous());
			
		
		}
	}

}
