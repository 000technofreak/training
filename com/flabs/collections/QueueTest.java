package com.flabs.collections;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

public class QueueTest {

	public static void main(String[] args) {

		Queue<Integer> q = new LinkedList<Integer>();
		
		q.poll();
		try {
			q.remove();
		} catch (NoSuchElementException nsee) {
			System.out.println(".....NoSuchElementException is thrown");
		}

		q.add(0);
		q.add(10);
		q.add(20);

		
		q.offer(30);
		q.offer(40);
		q.offer(50);
		System.out.println("\n the elements of queue q are " + q);

		
		System.out
				.println("\n element() : retrieves the first element of the queue "
						+ q.element());

		
		System.out.println("\n peek()  : Retrieves first element of the queue "
				+ q.peek());

		
		Integer rem = q.remove();
		System.out.println("element removed : " + rem);
		System.out
				.println("\n remove()  :  Remove head of the queue using remove() method and now the elements of the queue are \n "
						+ q);

		
		q.poll();
		System.out
				.println("\n poll()  :  Removes head of the queue using poll() method and the elements of the queue are  "
						+ q);
		System.out
				.println("\n after deleting the elements the size of the queue is "
						+ q.size());
	}
}
