package com.flabs.collections;

import java.util.IdentityHashMap;

public class IdentityHashMapTest {

	public static void main(String args[]) {
		
		IdentityHashMap<String, String> identityMap = new IdentityHashMap<String, String>();

		identityMap.put("sony", "bravia");
		identityMap.put(new String("sony"), "mobile");

		
		System.out.println("Size of IdentityHashMap: " + identityMap.size()); //2
		System.out.println("IdentityHashMap: " + identityMap);

		identityMap.put("sony", "videogame");

		System.out.println("Size of IdentityHashMap: " + identityMap.size()); //2
		System.out.println("IdentityHashMap: " + identityMap);

	}

}
