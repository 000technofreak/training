package com.flabs.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Vector;

public class VectorTest {
	
	public static void main(String args[]){
		
		Vector<Integer> vector = new Vector<>();
		vector.add(21);
		vector.add(43);
		vector.add(61);
		vector.add(24);
		vector.add(200);
		
		System.out.println(vector);
			
		
		vector.addElement(5000);
		
		System.out.println(vector.elementAt(4));
		vector.capacity();
		vector.trimToSize();
		vector.removeElementAt(4);
		
//		while(vector.iterator().hasNext())
//			System.out.println(vector.iterator().next());
//		
		for( Integer i : vector)
			System.out.println(i);
		
		
	}
	
	

}
