package com.flabs.Exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;




/*class MyException{
	public static void exception() throws testEx{
		throw (new testEx());
	}
	
	public static void main(String[] args){
		try{
			exception();
		}
		catch(testEx me){
			System.out.println("My Exception caught!");
		}
	}
*/

class MyException{

	private static Logger logger = Logger.getLogger("ABC");
 	void abc(){
		logger.setLevel(Level.SEVERE);
		logger.info("My Exception");

		throw new MyException2();
	}
	
	void def() throws MyException1{
		throw new MyException1();
	}
	
	public static void main(String args[]) throws MyException1{
		MyException me = new MyException();
		me.abc();
		me.def();
	}
}