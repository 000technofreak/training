package com.flabs.Exceptions;

public class DemoExceptions {

	//int a,b,c;
	public void give()
	{
		int a=0,b=100,c=0;
		try{
		a=0;
		b=100;
		c=b/a;
		System.out.println("In try:"+c);
		}
		
		catch(ArithmeticException ae){
			a=10;
			c=b/a;
			System.out.println("In catch:"+c);
		}
		
		catch(RuntimeException re){
			System.out.println("RUntime Exception");
		}
		
		finally{
			System.out.println("Value of C:"+c);
		}
		
		
			}
	public void give1() throws ArithmeticException{
		int d = 0;
		int f=100/d;
		System.out.println(f);
	}
	
	public static void main(String... args){
		DemoExceptions d = new DemoExceptions();
		d.give();
		try{
		d.give1();}
		
		catch(ArithmeticException ae){
			System.out.println("give1");
		}
	}
}
