package com.flabs.vehicle;

public class VehicleTest {

	public static void main(String args[])
	{
		Car c = new Car("BMW i3",2,"Electricity");
		Car c1 = new Car("Bugatti Veyron",2,"Diesel/Petrol");
		Truck t = new Truck("Mahindra Prime",2,"Diesel");
		
		c.getSpecs();
		c.move();
	}
}
