package com.flabs.vehicle;

public class Truck extends Vehicle {

String Name;
	
	Truck(String name,int seat, String fuel)
	{
		Name = name;
		seating = seat;
		fueltype = fuel;
	}
	
	public void getSpecs()
	{
		System.out.println("This Truck is "+Name+" and it has "+seating+" capacity and it takes "+fueltype+" as fuel!");
		
	}
	
	public void move()
	{
		System.out.println(Name+" is ready to move!");
	}
}
