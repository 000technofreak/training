package com.flabs.vehicle;

public class Car extends Vehicle{

	String Name;
	
	Car(String name,int seat, String fuel)
	{
		Name = name;
		seating = seat;
		fueltype = fuel;
	}
	
	public void getSpecs()
	{
		System.out.println("This Car is "+Name+" and it has "+seating+" seating capacity and it runs on "+fueltype+"!");
		
	}
	
	public void move()
	{
		System.out.println(Name+" is ready to move!");
	}
	
}
