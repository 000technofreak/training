package com.flabs.animal;

public abstract class Pet extends Animal {
	
	public boolean isWild(){
		return false;
	}
	
   public boolean isTamable(){
	   return true;
   }
}
