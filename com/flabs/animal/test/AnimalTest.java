package com.flabs.animal.test;
import com.flabs.animal.*;
public class AnimalTest {

	public static void main(String args[])
	{
		Cat c = new Cat("Martha","White");
		Dog d = new Dog("Spike","Black");
		Tiger t = new Tiger();
		Lion l = new Lion();
		Animal an = new Cat("Tom","Gray");
		Movable mv = new Dog("S","c");
		//Animal an = new Animal(); *Error
		Cat cat = (Cat)an; //reference conversion
		Dog dog = (Dog)mv;
		Cat ca = (Cat)mv;
		c.move();
		c.makesNoise();
		System.out.println(c.isWild()?"What? I'm Wild :O":"Yo! I ain't Wild!");
		d.makesNoise();
		d.move();
		System.out.println(d.isWild()?"Yo I'm a Pet Dog!":"I'm not Wild Yo! I'm a Pet dog!");
		t.makesNoise();
		t.move();
		l.makesNoise();
		l.move();
	
		
	}
}
