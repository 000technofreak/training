package com.flabs.animal;

public interface Tamable {

	boolean isTamable();
}
