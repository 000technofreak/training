package com.flabs.animal;

public interface Movable {

	abstract void move();
	
}
