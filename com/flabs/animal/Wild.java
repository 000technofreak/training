package com.flabs.animal;

public abstract class Wild extends Animal {

	
	public boolean isPet()
	{
		return false;
	}
	
	public boolean isTamable(){
		return false;
	}

}
