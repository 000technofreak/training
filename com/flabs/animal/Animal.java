package com.flabs.animal;

public abstract class Animal implements Movable{

	int legs;
	String color;
	
	public void move() {
		System.out.println("In Animal");
	}

	public abstract void makesNoise();
	
	public abstract boolean isWild();
	
	public abstract boolean isPet();
}
