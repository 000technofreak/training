package com.flabs.conversions;

public class WRCTest {

		
	public static void main(String... args){
		Parrot p = new Parrot();
		Bird bird = p;
		Object o = p;
		Parrot p1 = (Parrot) bird;
		//Parrot p2 = (Parrot) new Bird(); Runtime Exception
//		Parrot p3 = new Duck(); *TYpe mismatch*
//		Parrot p4 = (Parrot) new Duck(); *Error*
		p.makeSound();
		bird.makeSound();
		p1.makeSound();
		p.fly();
		Bird bird1 = new Parrot();// type 1
		flyable flyBird = (flyable) bird1;//type 2
		flyable flyparrot =  new Parrot(); //type 2
		Bird b3 = (Bird) flyparrot; //type-3
		flyable flyduck = new Duck();//type-4
		flyduck.fly();
		swimmable swim =  new Duck();//type-4
		swim.swim();
		flyBird.fly();
		flyparrot.fly();
	
	}
	
}

class Bird{
	Bird(){
		System.out.println("In Bird");
	}
	
	void flying(){
		System.out.println("FLying");
	}
	
	void makeSound(){
		System.out.println("Bird Sound");
	}
}
class Parrot extends Bird implements flyable{
	Parrot(){
		System.out.println("In Parrot");
	}
	
	@Override
	void makeSound(){
		System.out.println("Can speak");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Flyable Parrot");
	}
	
}

class Duck extends Bird implements flyable,swimmable{
	Duck(){
		System.out.println("In DUck");
	}
	
	void makeSound(){
		System.out.println("Quack");
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Duck Swim");
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println("Duck FLy");
	}
}

interface flyable{
	void fly();
}

interface swimmable{
	void swim();
}