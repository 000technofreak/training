
package com.flabs.bank;
import com.flabs.people.*;
public class Account {

	protected String accNo,accHolder,accAddress;
	protected double intRate;
	Account(String acc, String name)
	{
		accNo = acc;
		accHolder = name;
	}
	
	Account (Person p)
	{
		accHolder = p.getName();
		accAddress = p.getAddress();
		
	}
	
	String getName()
	{ return accHolder; }
	
	String getAddress()
	{ return accAddress; }
	public double getIntRate()
	{
		return intRate;
	}
	
	void setIntRate(double threshold)
	{
		intRate = threshold;
	}
	
}
