package com.flabs.innerclass;
/*
public class nonstatic {

	nonstatic(){
		System.out.println("non static");
	}
	class inner{
		inner(){
			
			System.out.println("Inner Class");
		}
		
		public void fun(){
			class outer{
				outer(){
					System.out.println("Local");
				}
			}
		}
	}
	
	public static void main(String args[]){
		nonstatic ns = new nonstatic();
		inner in = ns.new inner();
	}
}
*/

class AnonymousClassEx{
	Object obj = new Object(){
		String getClassName(){
			return this.getClass().getName();
		}
	};

	
Runnable run = new Runnable(){

	@Override
	public void run()
	{
		System.out.println("Running");
	}
};

}

