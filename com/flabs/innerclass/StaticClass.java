package com.flabs.innerclass;

public class StaticClass {
	//static StaticClass sc = new StaticClass();
	StaticClass(){
		System.out.println("COnstructor");
	}
	//static StaticClass sc = new StaticClass();


	static {
		//StaticClass sc = new StaticClass();
		System.out.println("Inside static class");
		//sc.getTest();
	}
	static StaticClass sc = new StaticClass();

	void getTest(){
		System.out.println("GetTest");
	}
	public static void main(String args[]){
		System.out.println("Inside Main Method");
		sc.getTest();
		StaticClass cc = new StaticClass();
	}
}
