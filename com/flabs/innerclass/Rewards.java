package com.flabs.innerclass;

public class Rewards {

	double getReward(double purchaseAmt){
		return purchaseAmt*RewardDetails.RewardRate;
	}
	
	static class RewardDetails{
		final static double RewardRate = 0.05;
	}
	
	public static void main(String args[]){
		System.out.println("Reward Rate="+RewardDetails.RewardRate);
		Rewards rew = new Rewards();
		System.out.println(rew.getReward(1000.00));
	}
}
