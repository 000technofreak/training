package com.flabs.innerclass;

class Car {
	
	Engine en = new Engine();
	public class Engine {
		int noOfCylinders = 4;
		int getCylinder(){
			return noOfCylinders;
		}
	}
	
	public static void main(String args[]){
       Car car = new Car();
       System.out.println(car.en.getCylinder());
	}
}