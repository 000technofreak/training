
package com.flabs.people;
import com.flabs.bank.*;
public class Person {

	private String Name;
	private String address;
	boolean awake;
	
	Person(String name, String add)
	{
		Name = name;
		address = add;
	}
	boolean isAwake()
	{
		return awake;
	}
	
	Account getAccount()
	{
		Account account = new Account(this);
		return account;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
}