import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

public class StateCapitalPopulation {


	public static void main(String[] args) {
		

		SortedMap<String,State> statePopulation = new TreeMap<String,State>(mycomp);

		State st = new State("Telangana", "Hyderabad", 4000);
		statePopulation.put(st.getState(), st);

		st = new State("Maharashtra", "Mumbai", 3000);
		statePopulation.put(st.getState(), st);

		st = new State("TamilNadu", "Chennai", 2000);
		statePopulation.put(st.getState(), st);

		System.out.println(statePopulation);

	}
	
	static Comparator<State> mycomp = new Comparator<State>() {


		@Override
		public int compare(State o1, State o2) {
			if ((o1.getPopulation() - o2.getPopulation()) == 0)
				return o1.getState().compareTo(o2.getState());
			if(o1.getPopulation() > o2.getPopulation())
				return 1;
		
				return -1;
		}

	};


}




