public class State {
	private String state, capital;
	private int population;

	State(String st, String cap, int pop) {
		setState(st);
		setCapital(cap);
		setPopulation(pop);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}
	

}
