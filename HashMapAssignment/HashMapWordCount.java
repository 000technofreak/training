import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HashMapWordCount {

	public static void main(String args[]) {

		Scanner k = new Scanner(System.in);

		System.out.println("Enter words:");
		String words = k.nextLine();
		String[] split = words.split(" ");
		Map<String, Integer> word = new HashMap<>();
		for (String s : split) {
			if (word.containsKey(s))
				word.put(s, word.get(s) + Integer.valueOf(1));
			else
				word.put(s, Integer.valueOf(1));
		}
		
	   System.out.println(word);

	}
}
